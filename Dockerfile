FROM node:10 as react-dev
WORKDIR /app
COPY  package.json yarn.lock ./
RUN yarn \
      --registry ... \
      --network-timeout 300000 \
      --frozen-lockfile

FROM react-dev AS react-build
COPY . ./
RUN yarn build

FROM ...
COPY nginx.conf /etc/nginx/vhosts/default.conf
COPY --from=react-build /app/build /www
COPY --from=react-build /app/.env.example /app/build-env-vars.sh /app/
WORKDIR /www

CMD ["/bin/bash", "-c", "/app/build-env-vars.sh /app/.env.example /www && nginx -g \"daemon off;\""]
