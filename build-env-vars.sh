#!/bin/bash

input_file="$1"
output_file="$2"/env-config.js

# Add assignment
echo "window._env_ = {" > $output_file

# Read each line in .env file
# Each line represents key=value pairs
while read -r line || [[ -n "$line" ]];
do
  # Split env variables by character `=`
  if printf '%s\n' "$line" | grep -q -e '='; then
    varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
    varvalue=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
  fi

  # skip comments
  if [[ $varname == "#"* ]]; then
    continue
  fi

  # Read value of current variable if exists as Environment variable
  value=$(printf '%s\n' "${!varname}")
  # Otherwise use value from .env file
  [[ -z $value ]] && value=${varvalue}

  echo "  $varname: '$value'," >> $output_file
  # Append configuration property to JS file
  # echo "  $varname: \"$value\"," >> $output_file
done < $input_file

echo "}" >> $output_file
