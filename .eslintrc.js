module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  // parserOptions: {
    // ecmaVersion: 2018,
    // sourceType: "module",
    // ecmaFeatures: {
    //   jsx: true
    // },
    // project: `./tsconfig.json`,
  // },
  plugins: [
    "react",
    "@typescript-eslint"
  ],
  extends: [
    "eslint:recommended",
    "airbnb-base",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:react/recommended"
  ],
  env: {
    browser: true,
    node: true,
    jest: true
  },
  settings: {
    react: {
      version: "detect"
    },
    "import/parsers": {
      "@typescript-eslint/parser": [
        ".ts",
        ".tsx"
      ]
    },
    "import/resolver": {
      "typescript": {}
    }
  },
  rules: {
    "@typescript-eslint/indent": [
      "error",
      2
    ],
    "no-console": "off",
    "class-methods-use-this": "off",
    "import/prefer-default-export": "off",
    "lines-between-class-members": "off",
    "react/prop-types": "off",
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/interface-name-prefix": "off",
    "@typescript-eslint/no-object-literal-type-assertion": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/no-non-null-assertion": "off",
    "@typescript-eslint/no-namespace": "off",
    "max-classes-per-file": "off",
    "quote-props": ["error", "consistent"],
  }
}

