#!/usr/bin/env bash

TAG=${1:-"latest"}

docker build -t ...:${TAG} -f Dockerfile . \
    && docker push ...:${TAG}