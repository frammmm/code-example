// tslint:disable jsx-no-lambda
import React from 'react';
import {
  Router as ReactRouter, Switch, Redirect, Route,
} from 'react-router-dom';
import {
  Agreement,
  Book,
  Branches,
  Branch,
  Collection,
  Edition,
  IssueSharedDocument,
  IssueRouter,
  Login,
  LibsList,
  Newspapers,
  Magazines,
  Periodicals,
  Search,
  Viewed,
  BookReviewsList,
} from 'pages';
import { history } from 'utils/history';
import { ScrollToTop } from 'components';
import {
  AuthLayout,
  AppRoute,
  HomeLayout,
} from 'containers';

export const Router = () => (
  <ReactRouter history={history}>
    <ScrollToTop>
      <Switch>
        <AppRoute
          path="/"
          exact
          layout={HomeLayout}
          layoutProps={{ showFooter: true }}
          component={Periodicals}
          access="private"
        />
        <AppRoute path="/login" layout={AuthLayout} component={Login} />
        <AppRoute
          path="/libs"
          layout={AuthLayout}
          component={LibsList}
          access="private"
        />
        <AppRoute path="/branches" exact layout={HomeLayout} component={Branches} access="private" />
        <AppRoute path="/branches/:branchId" layout={HomeLayout} component={Branch} access="private" />
        <AppRoute path="/newspapers" exact layout={HomeLayout} component={Newspapers} access="private" />
        <AppRoute path="/magazines" exact layout={HomeLayout} component={Magazines} access="private" />
        <AppRoute path="/editions/:editionId" exact layout={HomeLayout} component={Edition} access="private" />
        <AppRoute
          path="/editions/:editionId/issues/:issueId"
          layout={IssueRouter}
          access="private"
          layoutProps={{ bottomOffsetForIssue: true }}
        />
        <AppRoute
          path="/share/doc/:documentHash"
          layout={HomeLayout}
          layoutProps={{ showHeader: false, showFooter: true }}
          component={IssueSharedDocument}
          access="common"
        />
        <AppRoute path="/search" layout={HomeLayout} component={Search} access="private" />
        <AppRoute path="/collection" layout={HomeLayout} component={Collection} access="private" />
        <AppRoute path="/viewed" layout={HomeLayout} component={Viewed} access="private" />
        <AppRoute path="/agreement" layout={Agreement} access="common" />
        <AppRoute path="/books/:bookId" layout={HomeLayout} component={Book} access="private" />
        <AppRoute path="/books/" layout={HomeLayout} component={BookReviewsList} access="private" />
        <Route render={() => <Redirect to="/" />} />
      </Switch>
    </ScrollToTop>
  </ReactRouter>
);
