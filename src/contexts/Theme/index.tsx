import React from 'react';
import variables from 'styles/variables.scss';

const themeInitialState = {
  logoRegular: undefined, // ссылка на логотип 34x34(загружаемый файл)
  logoWide: undefined, // ссылка на логотип 200x34(загружаемый файл)
  primaryColor: variables.bluePrimary, // основной цввет
  primaryContrastingColor: variables.white, // контрастирующий с основным цвет
};

export const ThemeContext = React.createContext([themeInitialState, () => {}] as any);

interface IProps {
  children: React.ReactNode;
}

export const ThemeProvider = ({ children }: IProps) => {
  const state = React.useState(themeInitialState) as any;
  return (
    <ThemeContext.Provider value={state}>
      {children}
    </ThemeContext.Provider>
  );
};

export const useTheme = () => React.useContext(ThemeContext);
