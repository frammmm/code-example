interface Console {
  js: (...args: any[]) => {};
}

declare module '*.scss' {
  const content: {[className: string]: string};
  export = content;
}
