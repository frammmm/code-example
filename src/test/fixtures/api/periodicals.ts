export const periodicals = {
  fetchPeriodicals: {
    ok: '...',
  },
  fetchBranches: {
    ok: '...',
  },
  fetchBranch: {
    ok: '...',
  },
  fetchEdition: {
    ok: '...',
  },
  fetchEditionPeriodics: {
    ok: '...',
    pagination: {
      firstPage: '...',
      secondPage: '...',
      thirdPage: '...',
    },
  },
  fetchIssue: {
    ok: '...',
  },
  fetchNewspapers: {
    ok: '...',
  },
  fetchMagazines: {
    ok: '...',
  },
  fetchDocument: {
    ok: '...',
  },
  fetchShareLink: {
    ok: '...',
  },
  fetchCollection: {
    ok: '...',
  },
  fetchViewed: {
    ok: '...',
  },
};
