export const login = {
  submitEmail: {
    ok: {},
    wrongEmail: {
      code: 'email_not_allowed',
      message: '...',
    },
  },
  submitCode: {
    ok: {},
    wrongCode: {
      code: 'invalid_code',
      message: 'Неверный код доступа',
    },
  },
  logout: {
    ok: {},
  },
};
