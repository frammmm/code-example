import nock from 'nock';
import { toJS } from 'mobx';
import { RootStore } from 'stores';
import { Stores } from 'types';
import { env } from 'utils/env';
import * as fixtures from '../fixtures/api';

nock.disableNetConnect();

const mockApi = nock(env.REACT_APP_API_BASE_URL);

let store: Stores.IBooksStore;

describe('periodicalsStore', () => {
  beforeEach(() => {
    store = new RootStore().booksStore;
  });

  test('fetch book genres', async () => {
    mockApi
      .get('...')
      .query(true)
      .reply(200, fixtures.books.fetchBookGenres.ok);

    await store.fetchBookGenres();
    const { data, errors } = store.reviewGenresStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch book themes', async () => {
    mockApi
      .get('...')
      .query(true)
      .reply(200, fixtures.books.fetchBookThemes.ok);

    await store.fetchBookThemes();
    const { data, errors } = store.reviewBookThemesStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch book reviews', async () => {
    mockApi
      .get('...')
      .query(true)
      .reply(200, fixtures.books.fetchBookReviews.ok);

    await store.fetchReviewsPagesIfNeeded();
    const { data, errors } = store.reviewsPagesStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });
});
