import nock from 'nock';
import { toJS } from 'mobx';
import { RootStore } from 'stores';
import { env } from 'utils/env';
import * as fixtures from '../fixtures/api';

nock.disableNetConnect();

const mockApi = nock(env.REACT_APP_API_BASE_URL);

let store;

describe('authentication', () => {
  beforeEach(() => {
    localStorage.clear();
    store = new RootStore().authStore;
  });

  test('submit corerct email', async () => {
    const expectedData = fixtures.login.submitEmail.ok;
    mockApi
      .post('...')
      .reply(204, expectedData);

    await store.submitEmail({ email: 'test@test.test' });
    const { data, errors } = store.loginEmailStruct;
    expect(toJS(data)).toEqual(expectedData);
    expect(toJS(errors)).toBeUndefined();
    expect(store.isLoggedIn).toBe(false);
  });

  test('submit incorerct email', async () => {
    const expectedData = fixtures.login.submitEmail.wrongEmail;
    mockApi
      .post('...')
      .reply(403, expectedData);

    await store.submitEmail({ email: 'test@wrong.test' });
    const { data, errors } = store.loginEmailStruct;
    
    expect(toJS(data)).toBeUndefined();
    expect(errors.status).toEqual(403);
    expect(toJS(errors.data)).toEqual(expectedData);
    expect(store.isLoggedIn).toBe(false);
  });

  test('submit correct email and code', async () => {
    const expectedData = fixtures.login.submitCode.ok;
    mockApi
      .post('...')
      .reply(204, expectedData);

    await store.submitCode({ email: 'test@correct.test', code: '0000' });
    const { data, errors } = store.loginCodeStruct;

    expect(toJS(data)).toEqual(expectedData);
    expect(toJS(errors)).toBeUndefined();
    expect(store.isLoggedIn).toBe(true);
  });

  test('submit incorrect email and code', async () => {
    const expectedData = fixtures.login.submitCode.wrongCode;
    mockApi
      .post('...')
      .reply(400, expectedData);

    await store.submitCode({ email: 'test@correct.test', code: 'incorrect' });
    const { data, errors } = store.loginCodeStruct;

    expect(toJS(data)).toBeUndefined();
    expect(errors.status).toEqual(400);
    expect(toJS(errors.data)).toEqual(expectedData);
    expect(store.isLoggedIn).toBe(false);
  });

  test('logout', async () => {
    const expectedData = fixtures.login.logout.ok;
    mockApi
      .post('...')
      .reply(204, expectedData);

    await store.logout();
    const { data, errors } = store.logoutStruct;

    expect(toJS(data)).toEqual(expectedData);
    expect(toJS(errors)).toBeUndefined();
    expect(store.isLoggedIn).toBe(false);
  });
});
