import nock from 'nock';
import { toJS } from 'mobx';
import { RootStore } from 'stores';
import { Stores } from 'types';
import { env } from 'utils/env';
import * as fixtures from '../fixtures/api';

nock.disableNetConnect();

const mockApi = nock(env.REACT_APP_API_BASE_URL);

let store: Stores.IPeriodicalsStore;

describe('periodicalsStore', () => {
  beforeEach(() => {
    store = new RootStore().periodicalsStore;
  });

  test('fetch periodicals data', async () => {
    mockApi
      .get('...')
      .reply(200, fixtures.periodicals.fetchPeriodicals.ok);

    await store.fetchPeriodicals();
    const { data, errors } = store.periodicalsStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch branches data', async () => {
    mockApi
      .get('...')
      .query(true)
      .reply(200, fixtures.periodicals.fetchBranches.ok);

    await store.fetchBranches();
    const { data, errors } = store.branchesStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch branch data', async () => {
    mockApi
      .get('...')
      .reply(200, fixtures.periodicals.fetchBranch.ok);

    await store.fetchBranch('1');
    const { data, errors } = store.branchStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch edition data', async () => {
    mockApi
      .get('...')
      .query(true)
      .reply(200, fixtures.periodicals.fetchEdition.ok);

    await store.fetchEdition('1');
    const { data, errors } = store.editionStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch edition periodics data', async () => {
    mockApi
      .get('...')
      .query(true)
      .reply(200, fixtures.periodicals.fetchEditionPeriodics.ok);

    await store.fetchEditionPeriodicsIfNeeded('5');
    const { data, errors } = store.editionPeriodsStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch issue data', async () => {
    mockApi
      .get('...')
      .query(true)
      .reply(200, fixtures.periodicals.fetchIssue.ok);

    await store.fetchIssue('70');
    const { data, errors } = store.issueStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch newspapers data', async () => {
    mockApi
      .get('...')
      .query((query: any) => query.edition_type === 'newspaper')
      .reply(200, fixtures.periodicals.fetchNewspapers.ok);

    await store.fetchNewspapersIfNeeded();
    const { data, errors } = store.newspapersStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch magazines data', async () => {
    mockApi
      .get('...')
      .query((query: any) => query.edition_type === 'magazine')
      .reply(200, fixtures.periodicals.fetchMagazines.ok);

    await store.fetchMagazinesIfNeeded();
    const { data, errors } = store.magazinesStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch document data', async () => {
    mockApi
      .get('...')
      .reply(200, fixtures.periodicals.fetchDocument.ok);

    await store.fetchDocument('4336');
    const { data, errors } = store.documentStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch document share link', async () => {
    mockApi
      .post('...')
      .reply(200, fixtures.periodicals.fetchShareLink.ok);

    await store.fetchShareLink('4336');
    const { data, errors } = store.shareLinkStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch collection data', async () => {
    mockApi
      .get('...')
      .query(true)
      .reply(200, fixtures.periodicals.fetchCollection.ok);

    await store.fetchCollection();
    const { data, errors } = store.collectionStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  test('fetch viewed data', async () => {
    mockApi
      .get('...')
      .query(true)
      .reply(200, fixtures.periodicals.fetchViewed.ok);

    await store.fetchViewedIfNeeded();
    const { data, errors } = store.viewedStruct;

    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });

  describe('pagination', () => {
    test('fetch edition periodics paginated data', async () => {
      const collectedData = [];
      store.editionPeriods.limit = 1;

      mockApi
        .get('...')
        .query((query: any) => query.offset === '0')
        .reply(200, fixtures.periodicals.fetchEditionPeriodics.pagination.firstPage);

      await store.fetchEditionPeriodicsIfNeeded('5');

      expect(toJS(store.editionPeriodsStruct.errors)).toBeUndefined();
      expect(toJS(store.editionPeriodsStruct.data)).toMatchSnapshot();

      if (store.editionPeriodsStruct.data) {
        collectedData.push(...store.editionPeriodsStruct.data.data);
      }

      mockApi
        .get('...')
        .query((query: any) => query.offset === '1')
        .reply(200, fixtures.periodicals.fetchEditionPeriodics.pagination.secondPage);

      await store.fetchEditionPeriodicsIfNeeded('5');

      expect(toJS(store.editionPeriodsStruct.errors)).toBeUndefined();
      expect(toJS(store.editionPeriodsStruct.data)).toMatchSnapshot();

      if (store.editionPeriodsStruct.data) {
        collectedData.push(...store.editionPeriodsStruct.data.data);
      }

      mockApi
        .get('...')
        .query((query: any) => query.offset === '2')
        .reply(200, fixtures.periodicals.fetchEditionPeriodics.pagination.thirdPage);

      await store.fetchEditionPeriodicsIfNeeded('5');

      expect(toJS(store.editionPeriodsStruct.errors)).toBeUndefined();
      expect(toJS(store.editionPeriodsStruct.data)).toMatchSnapshot();

      if (store.editionPeriodsStruct.data) {
        collectedData.push(...store.editionPeriodsStruct.data.data);
      }

      expect(collectedData).toEqual(store.editionPeriods.data);
      expect(store.editionPeriods.isLastPageFetched).toEqual(true);

      store.fetchEditionPeriodics = jest.fn();

      await store.fetchEditionPeriodicsIfNeeded('5');

      expect(store.fetchEditionPeriodics).not.toBeCalled();
    });

    test('fetch paginated data with error', async () => {
      store.editionPeriods.limit = 1;

      mockApi
        .get('...')
        .query((query: any) => query.offset === '0')
        .replyWithError({});

      await store.fetchEditionPeriodicsIfNeeded('5');

      expect(toJS(store.editionPeriodsStruct.data)).toBeUndefined();
      expect(store.editionPeriodsStruct.errors).toBeInstanceOf(Error);
    });
  });
});
