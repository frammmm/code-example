import nock from 'nock';
import { toJS } from 'mobx';
import { RootStore } from 'stores';
import { env } from 'utils/env';
import * as fixtures from '../fixtures/api';

nock.disableNetConnect();

const mockApi = nock(env.REACT_APP_API_BASE_URL);

let store;

describe('client', () => {
  beforeEach(() => {
    localStorage.clear();
    store = new RootStore().clientStore;
  });

  test('GET client_info', async () => {
    const expectedData = fixtures.client.info.ok;
    mockApi
      .get('...')
      .reply(200, expectedData);

    await store.fetchClientInfo();
    const { data, errors } = store.clientInfoStruct;
    
    expect(toJS(errors)).toBeUndefined();
    expect(toJS(data)).toMatchSnapshot();
  });
});
