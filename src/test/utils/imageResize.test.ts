import { makeResizeUrl } from 'utils/imageResize';

describe('image resizes', () => {
  test('from url with resize parameters', () => {
    const url = '...';
    expect(makeResizeUrl(url, 368)).toBe('...');
  });

  test('from url without resize parameters', () => {
    const url = '...';
    expect(makeResizeUrl(url, 128)).toBe('...');
  });

  test('from url without query', () => {
    const url = '...';
    expect(makeResizeUrl(url, 128)).toBe('...');
  });

  test('WxH from url with query WxH', () => {
    const url = '...';
    expect(makeResizeUrl(url, 160, 130)).toBe('...');
  });

  test('W from url with query WxH', () => {
    const url = '...';
    expect(makeResizeUrl(url, 160)).toBe('...');
  });
});
