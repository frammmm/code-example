import axios from 'axios';
import qs from 'qs';
import * as _ from 'lodash';
import { camelizeKeys, decamelizeKeys } from 'humps';
import { env } from 'utils/env';
import NetworkError from './networkError';

const responseTransformers = [
  ...(axios.defaults.transformResponse as any),
  (data: any) => camelizeKeys(data),
];

const requestTransformers = [
  (data: any) => decamelizeKeys(data),
  ...(axios.defaults.transformRequest as any),
];

const client = axios.create({
  // eslint-disable-next-line no-underscore-dangle
  baseURL: env.REACT_APP_API_BASE_URL,
  transformResponse: responseTransformers,
  paramsSerializer: (params: any) => qs.stringify(params, { arrayFormat: 'comma' }),
});

client.interceptors.response.use(
  (x) => x,
  (error) => {
    if (process.env.NODE_ENV !== 'test') {
      console.error(error);
    }
    if (!error.response) {
      const typedError = error.isAxiosError ? new Error(error) : error;
      return Promise.reject(typedError);
    }
    const convertedError = new NetworkError(error.response || {});
    return Promise.reject(convertedError);
  },
);

export { client };

const mergeAxiosOpts = ({
  transformRequest = _.identity,
  transformResponse = _.identity,
  params = {},
  ...opts
}: any = {}) => ({
  transformRequest: [transformRequest, ...requestTransformers],
  transformResponse: [...responseTransformers, transformResponse],
  params: decamelizeKeys(params),
  ...opts,
});

export default {
  get: (url: string, opts?: any): Promise<any> => (
    client.get(url, mergeAxiosOpts(opts))
  ),
  delete: (url: string, opts?: any): Promise<any> => (
    client.delete(url, mergeAxiosOpts(opts))
  ),
  post: (url: string, data: any, opts?: any): Promise<any> => (
    client.post(url, data, mergeAxiosOpts(opts))
  ),
  put: (url: string, data: any, opts?: any): Promise<any> => (
    client.put(url, data, mergeAxiosOpts(opts))
  ),
  patch: (url: string, data: any, opts?: any): Promise<any> => (
    client.patch(url, data, mergeAxiosOpts(opts))
  ),
};
