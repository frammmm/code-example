import React from 'react';
import { Provider } from 'mobx-react';
import 'intersection-observer'; // polyfill
import { RootStore } from 'stores';
import { ClientInfoHandler, ThemeHandler } from 'containers';
import { ThemeProvider } from 'contexts';
import { Sentry } from 'utils/sentry';
import { Router } from './Router';
import './styles/global.scss';

const rootStore = new RootStore() as any;

if (process.env.NODE_ENV === 'development') {
  console.log(rootStore.stores);
}

class App extends React.Component {
  componentDidCatch(e: Error) {
    Sentry.captureException(e);
    console.error(e);
  }

  render() {
    return (
      <Provider {...rootStore.stores}>
        <ThemeProvider>
          <ClientInfoHandler />
          <ThemeHandler />
          <Router />
        </ThemeProvider>
      </Provider>
    );
  }
}

export default App;
