import { Struct } from 'utils/struct';
import { PaginalItem } from 'utils/paginalItem';

export namespace Api {
  export interface Issue {
    id: number;
    cover: string;
    date: string;
    url: string;
    isViewed: boolean;
    edition: {
      name: string;
      id: number;
    };
  }

  export interface IssueResponse {
    data: Issue[];
    total: number;
  }

  export interface Edition {
    id: number;
    isCollected: boolean;
    name: string;
    url: string;
    latestIssue: {
      cover: string;
    };
  }

  export interface Branch {
    editions: Edition[];
    id: number;
    name: string;
    url: string;
  }

  export interface BranchListItem {
    id: number;
    name: string;
    url: string;
    latestIssue: {
      cover: string;
    };
  }

  export interface BranchResponse {
    data: Branch;
  }

  export interface BranchesResponse {
    branches: Branch[];
    total: number;
  }

  export interface BranchesTransformedResponse {
    data: Branch[];
    total: number;
  }

  export interface PeriodicalsResponse {
    branches: BranchListItem[];
    collectionLatest: Issue[];
    latestMagazines: Issue[];
    latestNewspapers: Issue[];
  }

  export interface EditionPeriod {
    startDate: string;
    endDate: string;
    index: number;
    state: string;
    issues: Issue[];
  }

  export interface EditionPeriodsResponse {
    data: EditionPeriod[];
  }

  export interface EditionResponse {
    data: {
      description: string;
      id: number;
      isCollected: boolean;
      latestIssue: Issue;
      logo: string;
      name: string;
      periodicity: string;
    };
  }

  export interface Review {
    id: number;
    cover: string;
    video: string;
    url: string;
    book: {
      id: number;
      title: string;
      author: string;
      cover: string;
    };
  }

  export interface ReviewsResponse {
    reviews: {
      hasNext: boolean;
      data: Review[];
    };
  }

  export interface BookGenre {
    id: number;
    name: string;
  }

  export interface BookTheme {
    id: number;
    name: string;
  }

  export namespace Search {
    export interface Highlight {
      annotation: string;
      body: string[];
      title: string[];
    }

    export interface Document {
      id: number;
      title: string;
      pages: number[];
      highlight: Highlight;
      issue: {
        id: number;
        name: string;
        date: string;
        number: string;
        url: string;
        edition: {
          id: number;
          name: string;
          url: string;
        };
      };
    }

    export interface Response {
      data: {
        total: number;
        editions: Edition[];
        documents: Search.Document[];
      };
    }
  }


  export namespace IssueDetails {
    export interface Coordinate {
      top: number;
      right: number;
      bottom: number;
      left: number;
      page: number;
    }

    export interface Document {
      id: number;
      title: string;
      annotation: string;
      authors: string;
      pages: number[];
      pagesVisual: string[];
      coordinates: Coordinate[];
    }

    export interface Edition {
      id: number;
      logo: string;
      name: string;
    }

    export interface Response {
      id: number;
      number: string;
      date: string;
      edition: Edition;
      documents: Search.Document[];
      pages: string[];
      thumbnails: string[];
    }
  }

  export interface DocumentResponse {
    id: number;
    annotation: string;
    authors: string;
    body: string;
    title: string;
    pages: number[];
  }

  export interface ShareLinkResponse {
    link: string;
  }

  export interface SharedDocumentResponse {
    id: number;
    editionId: number;
    issueId: number;
    annotation: string;
    authors: string;
    body: string;
    title: string;
    pages: number[];
  }

  export interface CollectionResponse {
    data: Edition[];
  }

  export namespace Book {
    export type Vote = null | 'like' | 'dislike';

    export interface Response {
      id: number;
      title: string;
      author: string;
      cover: string;
      readTime: null;
      publisher: string;
      description: string;
      genres: string[];
      themes: string[];
      vote: Vote;
      likes: number;
      dislikes: number;
      bookUrl: string;
      reviewId: number;
      reviewVideo: string;
    }

    export interface VoteResponse {
      reviewVideo: string;
    }
  }

  export namespace ClientInfo {
    export interface Response {
      asdf: string;
    }
  }
}

export namespace Stores {
  export interface IPeriodicalsStore {
    branchStruct: Struct<Api.BranchResponse, any>;
    branchesStruct: Struct<Api.BranchesTransformedResponse, any>;
    collectionStruct: Struct<Api.CollectionResponse, any>;
    documentStruct: Struct<Api.DocumentResponse, any>;
    editionStruct: Struct<Api.EditionResponse, any>;
    editionCollectStruct: Struct<any, any>;
    editionPeriodsStruct: Struct<Api.EditionPeriodsResponse, any>;
    issueStruct: Struct<Api.IssueDetails.Response, any>;
    newspapersStruct: Struct<Api.IssueResponse, any>;
    magazinesStruct: Struct<Api.IssueResponse, any>;
    periodicalsStruct: Struct<Api.PeriodicalsResponse, any>;
    shareLinkStruct: Struct<Api.ShareLinkResponse, any>;
    sharedDocumentStruct: Struct<Api.SharedDocumentResponse, any>;
    viewedStruct: Struct<Api.EditionPeriodsResponse, any>;

    branches: PaginalItem<Api.Branch>;
    editionPeriods: PaginalItem<Api.EditionPeriod>;
    newspapers: PaginalItem<Api.Issue>;
    magazines: PaginalItem<Api.Issue>;
    viewed: PaginalItem<Api.EditionPeriod>;

    fetchBranch: (branchId: string) => Promise<void>;
    fetchBranches: () => Promise<Struct<Api.BranchesTransformedResponse, any>>;
    fetchBranchesIfNeeded: () => Promise<void>;
    fetchCollection: () => Promise<void>;
    fetchEdition: (editionId: string) => Promise<void>;
    fetchEditionPeriodics: (editionId: string) => Promise<Struct<Api.EditionPeriodsResponse, any>>;
    fetchEditionPeriodicsIfNeeded: (editionId: string) => Promise<void>;
    fetchDocument: (id: string) => Promise<void>;
    fetchIssue: (issueId: string) => Promise<void>;
    fetchNewspapersIfNeeded: () => Promise<void>;
    fetchMagazinesIfNeeded: () => Promise<void>;
    fetchPeriodicals: () => Promise<void>;
    fetchShareLink: (id: string) => Promise<void>;
    fetchSharedDocument: (hash: string) => Promise<void>;
    fetchViewedIfNeeded: () => Promise<void>;
    toggleEditionCollectionState: (editionId: string, nextState: boolean) => Promise<void>;
    clearStructs: () => void;
  }

  export interface IBooksStore {
    bookStruct: Struct<Api.Book.Response, any>;
    bookVoteStruct: Struct<Api.Book.VoteResponse, any>;
    reviewsPagesStruct: Struct<Api.ReviewsResponse, any>;
    reviewGenresStruct: Struct<Api.BookGenre[], any>;
    reviewBookThemesStruct: Struct<Api.BookTheme[], any>;

    reviewsPages: PaginalItem<Api.Review>;

    fetchBook: (bookId: string) => Promise<void>;
    fetchBookGenres: () => Promise<void>;
    fetchBookThemes: () => Promise<void>;
    fetchReviewsPagesIfNeeded: (params?: object) => Promise<void>;
    rateBook: (p: { bookId: string; vote: Api.Book.Vote; alreadyVoted: boolean }) => Promise<void>;
    clearStructs: () => void;
  }

  export interface IAuthStore {
    isLoggedIn: boolean;
    temporaryEmail?: string;
    loginEmailStruct: Struct<any, any>;
    loginCodeStruct: Struct<any, any>;
    logoutStruct: Struct<any, any>;
  }

  export interface IClientStore {
    clientInfoStruct: Struct<any, any>;

    fetchClientInfo: () => Promise<void>;
  }
}
