import * as _ from 'lodash';

export const makeResizeUrl = (url: string, width: number, height?: number) => {
  const heightStr = height === undefined ? '-' : `${height}`;
  const size = `${width}x${heightStr}`;
  const normalizedUrl = _.startsWith(url, 'http') ? url : `http:${url}`;
  const { search, pathname, host } = new URL(normalizedUrl);
  const pathElements = pathname.split('/');
  const resizeParamIdx = pathElements.indexOf('resize');
  if (resizeParamIdx !== -1) {
    const newPathnameElements = pathElements.map(
      (v, idx) => ((resizeParamIdx + 1 === idx) ? size : v),
    );
    const newPathname = newPathnameElements.join('/');
    return `//${host}${newPathname}${search}`;
  }
  return `//${host}${pathname}/resize/${size}${search}`;
};
