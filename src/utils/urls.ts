import qs from 'qs';
import { Api } from 'types';

type MakeIssueDocUrl = {
  editionId: number | string;
  issueId: number | string;
  id: number | string;
};

export const makeIssueDocUrl = ({ editionId, issueId, id }: MakeIssueDocUrl) => (
  `/editions/${editionId}/issues/${issueId}/?view=doc&id=${id}`
);

export const addIssueUrl = (issue: Api.Issue) => ({
  ...issue,
  url: `/editions/${issue.edition.id}/issues/${issue.id}`,
});

export const addEditionUrl = (edition: Api.Edition) => ({
  ...edition,
  url: `/editions/${edition.id}/`,
});

export const addBranchUrl = (branch: Api.Branch) => ({
  ...branch,
  url: `/branches/${branch.id}/`,
  editions: branch.editions.map(addEditionUrl),
});

export const addBranchListItemUrl = (item: Api.BranchListItem) => ({
  ...item,
  url: `/branches/${item.id}/`,
});

export const addBookReviewItemUrl = (review: Api.Review) => ({
  ...review,
  url: `/books/${review.id}/`,
});

export const getQueryParams = (search: string): any => {
  if (!search) {
    return {};
  }
  return qs.parse(search.slice(1));
};
