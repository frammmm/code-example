import { toJS } from 'mobx';

console.js = function consoleJsCustom(...args: any[]): any {
  return console.log(...args.map((arg) => toJS(arg)));
};
