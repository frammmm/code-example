import { decamelizeKeys } from 'humps';
import { env } from 'utils/env';
import { statTypes } from './analytics';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const mp = require('mixpanel-browser');

const token = env.REACT_APP_MIXPANEL_TOKEN;
const enabled = !!token;

if (enabled) {
  mp.init(token);
}

export const mixpanel = {
  identify: (id: any) => {
    if (enabled) mp.identify(id);
  },
  alias: (id: any) => {
    if (enabled) mp.alias(id);
  },
  track: (name: statTypes, props: any = {}) => {
    console.log(name, props);
    if (enabled) mp.track(name, decamelizeKeys(props));
  },
  people: {
    set: (props: any) => {
      if (enabled) mp.people.set(decamelizeKeys(props));
    },
  },
};
