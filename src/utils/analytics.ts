import { decamelizeKeys } from 'humps';
import { client } from 'api';

// exit - уход с любой из страниц издания
export type statTypes =
  | 'spreads'
  | 'documents'
  | 'spread'
  | 'document'
  | 'picture'
  | 'exit'
  | 'share_link_create'
  | 'share_link_visit';

export const trackStatEvent = (eventName: statTypes, payload?: { [key: string]: any }) => {
  const requestPayload = decamelizeKeys({
    type: eventName,
    ...payload,
  });

  try {
    client.post(
      '...',
      JSON.stringify(requestPayload),
      {
        withCredentials: true,
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
      },
    );
  } catch (e) {
    console.error(e);
  }
};
