import * as Sentry from '@sentry/browser';
import { env } from 'utils/env';

export { Sentry };

export const initSentry = () => {
  if (process.env.NODE_ENV === 'production') {
    if (env.REACT_APP_SENTRY_ENV && env.REACT_APP_SENTRY_DSN) {
      Sentry.init({
        environment: env.REACT_APP_SENTRY_ENV,
        dsn: env.REACT_APP_SENTRY_DSN,
      });
    }
  }
};
