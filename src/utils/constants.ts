export const Breakpoints = {
  xxs: 320,
  xs: 576,
  sm: 768,
  md: 992,
  lg: 1200,
};

export const KeyCodes = {
  arrowLeft: 37,
  arrowRight: 39,
  enter: 13,
};
