/* eslint-disable no-param-reassign */
import * as _ from 'lodash';
import { action } from 'mobx';
import { toast } from '../components/toast';

export interface Struct<T, E> {
  data?: T;
  isFetching: boolean;
  errors?: E;
}

interface StructFlowOpts<T> {
  transformResponse(d: T): T;
}

const defaultOpts = {
  transformResponse: (d: any) => d,
};

export const getDefaultStruct = <T>(): Struct<T, any> => ({
  data: undefined,
  isFetching: false,
  errors: undefined,
});

export async function structFlow<T>(
  struct: Struct<T, any>,
  apiCall: () => Promise<any>,
  opts: StructFlowOpts<T> = defaultOpts,
): Promise<any> {
  return action(async () => {
    struct.data = undefined;
    struct.isFetching = true;
    struct.errors = undefined;
    try {
      const { data } = await apiCall();
      const { transformResponse } = opts;
      struct.data = transformResponse(data) || ({} as any);
    } catch (error) {
      if (process.env.NODE_ENV === 'development') {
        console.error(error);
      }
      if (_.endsWith(error.message, 'Network Error')) {
        toast.error('Ошибка сети. Проверьте подключение');
      }
      struct.errors = error;
    } finally {
      struct.isFetching = false;
    }
  })();
}
