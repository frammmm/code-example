import { Struct } from './struct';

export interface PaginalItem<T> {
  data: Array<T>;
  id?: string | undefined;
  limit: number;
  page: number;
  isLastPageFetched: boolean;
  reseted: boolean;
  reset(): void;
}

interface PaginalItemOptions {
  limit?: number;
}

export const getPaginalItem = <T>(options?: PaginalItemOptions): PaginalItem<T> => {
  const defaultOptions = {
    data: [],
    limit: 12,
    page: 1,
    isLastPageFetched: false,
    reseted: false,
    reset() {
      this.data = [];
      this.page = 1;
      this.isLastPageFetched = false;
      this.reseted = true;
    },
  };

  return {
    ...defaultOptions,
    ...options,
  };
};

interface FetchPaginalItemOptions {
  fetchFunction: () => Promise<Struct<any, any>>;
  item: PaginalItem<any>;
}

export const fetchPaginalData = async (options: FetchPaginalItemOptions) => {
  const { fetchFunction, item } = options;

  if (item.isLastPageFetched) {
    return;
  }

  const { data, errors } = await fetchFunction();

  if (errors || !data) {
    item.isLastPageFetched = true;

    return;
  }

  item.reseted = false;

  const structData = data.data;

  item.page += 1;
  item.data.push(...structData);

  if (!structData
    || !structData.length
    || structData.length < item.limit
    || data.hasNext === false
  ) {
    item.isLastPageFetched = true;
  }
};
