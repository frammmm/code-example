import { observable, action } from 'mobx';
import _ from 'lodash';
import api from 'api';
import { getDefaultStruct, structFlow } from 'utils/struct';
import { Api } from 'types';
import { addIssueUrl, addEditionUrl } from 'utils/urls';

export class SearchStore {
  private root: { [key: string]: any };
  @observable public searchFormStruct = getDefaultStruct<Api.Search.Response>();

  public constructor(stores: any) {
    this.root = stores;
  }

  @action.bound
  public search = async (query: string) => {
    await structFlow<Api.Search.Response>(
      this.searchFormStruct,
      () => api.get('...', {
        withCredentials: true,
        params: {
          limit: 10,
          offset: 0,
          query,
        },
      }),
      {
        transformResponse: ({ data: { editions, documents, ...rest } }: Api.Search.Response) => ({
          data: {
            ...rest,
            editions: editions.map(addEditionUrl),
            documents: documents.map((doc: Api.Search.Document) => ({
              ...doc,
              issue: {
                ...addIssueUrl(doc.issue as any) as any,
                edition: addEditionUrl(doc.issue.edition as any) as any,
              },
            })),
          },
        }),
      },
    );

    if (_.get(this.searchFormStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };
}
