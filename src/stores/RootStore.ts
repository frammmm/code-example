import { AuthStore } from './AuthStore';
import { BooksStore } from './BooksStore';
import { ClientStore } from './ClientStore';
import { PeriodicalsStore } from './PeriodicalsStore';
import { SearchStore } from './SearchStore';

export class RootStore {
  public stores = {};

  public constructor() {
    const storesMap = {
      authStore: AuthStore,
      booksStore: BooksStore,
      clientStore: ClientStore,
      periodicalsStore: PeriodicalsStore,
      searchStore: SearchStore,
    };

    // create instances for all stores and bind them to RootStore instance
    Object.entries(storesMap).forEach(([name, Target]) => {
      const instance = new Target(this.stores);
      (this as any)[name] = instance;
      (this.stores as any)[name] = instance;
    });
  }
}
