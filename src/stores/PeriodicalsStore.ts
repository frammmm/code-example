import { observable, action } from 'mobx';
import _ from 'lodash';
import api from 'api';
import { getDefaultStruct, structFlow } from 'utils/struct';
import { Api, Stores } from 'types';
import {
  addIssueUrl,
  addBranchUrl,
  addBranchListItemUrl,
  addEditionUrl,
} from 'utils/urls';
import { fetchPaginalData, getPaginalItem } from 'utils/paginalItem';

export class PeriodicalsStore implements Stores.IPeriodicalsStore {
  private root: { [key: string]: any };
  @observable public periodicalsStruct = getDefaultStruct<Api.PeriodicalsResponse>();
  @observable public branchStruct = getDefaultStruct<Api.BranchResponse>();
  @observable public branchesStruct = getDefaultStruct<Api.BranchesTransformedResponse>();
  @observable public newspapersStruct = getDefaultStruct<Api.IssueResponse>();
  @observable public magazinesStruct = getDefaultStruct<Api.IssueResponse>();
  @observable public editionStruct = getDefaultStruct<Api.EditionResponse>();
  @observable public editionPeriodsStruct = getDefaultStruct<Api.EditionPeriodsResponse>();
  @observable public editionCollectStruct = getDefaultStruct();
  @observable public collectionStruct = getDefaultStruct<Api.CollectionResponse>();
  @observable public issueStruct = getDefaultStruct<Api.IssueDetails.Response>();
  @observable public documentStruct = getDefaultStruct<Api.DocumentResponse>();
  @observable public shareLinkStruct = getDefaultStruct<Api.ShareLinkResponse>();
  @observable public sharedDocumentStruct = getDefaultStruct<Api.SharedDocumentResponse>();
  @observable public viewedStruct = getDefaultStruct<Api.EditionPeriodsResponse>();

  @observable public branches = getPaginalItem<Api.Branch>();
  @observable public editionPeriods = getPaginalItem<Api.EditionPeriod>();
  @observable public newspapers = getPaginalItem<Api.Issue>();
  @observable public magazines = getPaginalItem<Api.Issue>();
  @observable public viewed = getPaginalItem<Api.EditionPeriod>();

  public constructor(stores: any) {
    this.root = stores;
  }

  @action.bound
  public clearStructs = () => {
    this.periodicalsStruct = getDefaultStruct();
    this.branchStruct = getDefaultStruct();
    this.branchesStruct = getDefaultStruct();
    this.newspapersStruct = getDefaultStruct();
    this.magazinesStruct = getDefaultStruct();
    this.editionStruct = getDefaultStruct();
    this.editionPeriodsStruct = getDefaultStruct();
    this.editionCollectStruct = getDefaultStruct();
    this.collectionStruct = getDefaultStruct();
    this.issueStruct = getDefaultStruct();
    this.documentStruct = getDefaultStruct();
    this.shareLinkStruct = getDefaultStruct();
    this.sharedDocumentStruct = getDefaultStruct();
    this.viewedStruct = getDefaultStruct();
  }

  @action.bound
  public fetchPeriodicals = async () => {
    await structFlow<Api.PeriodicalsResponse>(
      this.periodicalsStruct,
      () => api.get('/v1/main/', {
        withCredentials: true,
      }),
      {
        transformResponse: ({
          latestMagazines, latestNewspapers, collectionLatest, branches, ...rest
        }: Api.PeriodicalsResponse) => ({
          ...rest,
          branches: branches.map(addBranchListItemUrl),
          collectionLatest: collectionLatest.map(addIssueUrl),
          latestMagazines: latestMagazines.map(addIssueUrl),
          latestNewspapers: latestNewspapers.map(addIssueUrl),
        }),
      },
    );

    if (_.get(this.periodicalsStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };

  @action.bound
  public fetchBranches = async () => {
    const { page, limit } = this.branches;
    const offset = page * limit - limit;

    await structFlow<Api.BranchesTransformedResponse>(
      this.branchesStruct,
      () => api.get('...', {
        params: { offset, limit },
        withCredentials: true,
      }),
      {
        transformResponse: ({ branches, ...rest }: any) => ({
          ...rest,
          data: branches.map(addBranchUrl),
        }),
      },
    );

    if (_.get(this.branchesStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }

    return this.branchesStruct;
  };

  @action.bound
  public fetchBranchesIfNeeded = async () => {
    await fetchPaginalData({
      fetchFunction: () => this.fetchBranches(),
      item: this.branches,
    });
  }

  @action.bound
  public fetchBranch = async (branchId: string) => {
    await structFlow<Api.BranchResponse>(
      this.branchStruct,
      () => api.get(`...`, { withCredentials: true }),
      {
        transformResponse: ({ data, ...rest }: Api.BranchResponse) => ({
          ...rest,
          data: addBranchUrl(data),
        }),
      },
    );

    if (_.get(this.branchStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };

  @action.bound
  public fetchEdition = async (editionId: string) => {
    await structFlow<Api.EditionResponse>(
      this.editionStruct,
      () => api.get(`...`, {
        params: { _: Date.now() },
        withCredentials: true,
      }),
      {
        transformResponse: ({ data }: Api.EditionResponse) => ({ data }),
      },
    );

    if (_.get(this.editionStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };

  @action.bound
  public fetchEditionPeriodics = async (editionId: string) => {
    const { page, limit } = this.editionPeriods;
    const offset = page * limit - limit;

    await structFlow<Api.EditionPeriodsResponse>(
      this.editionPeriodsStruct,
      () => api.get(`...`, {
        params: { offset, limit },
        withCredentials: true,
      }),
      {
        transformResponse: ({ data }: Api.EditionPeriodsResponse) => ({
          data: data.map(
            ({ issues, ...rest }: Api.EditionPeriod) => ({
              issues: issues.map(addIssueUrl),
              ...rest,
            }),
          ),
        }),
      },
    );

    if (_.get(this.editionPeriodsStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }

    return this.editionPeriodsStruct;
  };

  @action.bound
  public fetchEditionPeriodicsIfNeeded = async (editionId: string) => {
    if (this.editionPeriods.id !== editionId) {
      this.editionPeriods.data = [];
      this.editionPeriods.isLastPageFetched = false;
      this.editionPeriods.page = 1;
    }

    this.editionPeriods.id = editionId;

    await fetchPaginalData({
      fetchFunction: () => this.fetchEditionPeriodics(editionId),
      item: this.editionPeriods,
    });
  }

  @action.bound
  public toggleEditionCollectionState = async (editionId: string, nextCollectedState: boolean) => {
    await structFlow(
      this.editionCollectStruct,
      () => {
        if (nextCollectedState) {
          return api.put('...', {}, {
            params: { editionId },
            withCredentials: true,
          });
        }

        return api.delete('...', {
          params: { editionId },
          withCredentials: true,
        });
      },
    );

    if (_.get(this.editionCollectStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };

  @action.bound
  public fetchIssue = async (issueId: string) => {
    await structFlow<Api.IssueDetails.Response>(
      this.issueStruct,
      () => api.get(`...`, {
        params: { offset: 0, limit: 12, _: Date.now() },
        withCredentials: true,
      }),
      {
        transformResponse: ({ data }: any) => ({
          ...data,
          edition: addEditionUrl(data.edition),
        }),
      },
    );

    if (_.get(this.issueStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };

  @action.bound
  public fetchNewspapers = async () => {
    const { page, limit } = this.newspapers;
    const offset = page * limit - limit;

    await structFlow<Api.IssueResponse>(
      this.newspapersStruct,
      () => api.get('...', {
        withCredentials: true,
        params: {
          editionType: 'newspaper',
          offset,
          limit,
        },
      }),
      {
        transformResponse: ({ data, ...rest }: Api.IssueResponse) => ({
          ...rest,
          data: data.map(addIssueUrl),
        }),
      },
    );

    if (_.get(this.newspapersStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }

    return this.newspapersStruct;
  };

  @action.bound
  public fetchNewspapersIfNeeded = async () => {
    await fetchPaginalData({
      fetchFunction: () => this.fetchNewspapers(),
      item: this.newspapers,
    });
  }

  @action.bound
  public fetchMagazines = async () => {
    const { page, limit } = this.magazines;
    const offset = page * limit - limit;

    await structFlow<Api.IssueResponse>(
      this.magazinesStruct,
      () => api.get('...', {
        withCredentials: true,
        params: {
          editionType: 'magazine',
          offset,
          limit,
        },
      }),
      {
        transformResponse: ({ data, ...rest }: Api.IssueResponse) => ({
          ...rest,
          data: data.map(addIssueUrl),
        }),
      },
    );

    if (_.get(this.magazinesStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }

    return this.magazinesStruct;
  };

  @action.bound
  public fetchMagazinesIfNeeded = async () => {
    await fetchPaginalData({
      fetchFunction: () => this.fetchMagazines(),
      item: this.magazines,
    });
  }

  @action.bound
  public fetchDocument = async (id: string) => {
    await structFlow<Api.DocumentResponse>(
      this.documentStruct,
      () => api.get(`...`, {
        withCredentials: true,
      }),
      {
        transformResponse: ({ data }: any) => data,
      },
    );

    if (_.get(this.documentStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };

  @action.bound
  public fetchShareLink = async (id: string) => {
    await structFlow<Api.ShareLinkResponse>(
      this.shareLinkStruct,
      () => api.post(`...`, {
        withCredentials: true,
      }),
    );

    if (_.get(this.shareLinkStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };

  @action.bound
  public fetchSharedDocument = async (hash: string) => {
    await structFlow<Api.SharedDocumentResponse>(
      this.sharedDocumentStruct,
      () => api.get(`...`, {
        withCredentials: true,
      }),
      {
        transformResponse: ({ data }: any) => data,
      },
    );

    if (_.get(this.sharedDocumentStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };

  @action.bound
  public fetchCollection = async () => {
    await structFlow<Api.CollectionResponse>(
      this.collectionStruct,
      () => api.get('...', {
        params: { offset: 0, limit: 12 },
        withCredentials: true,
      }),
      {
        transformResponse: ({ data, ...rest }: Api.CollectionResponse) => ({
          ...rest,
          data: data.map(addEditionUrl),
        }),
      },
    );

    if (_.get(this.collectionStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };

  @action.bound
  public fetchViewed = async () => {
    const { page, limit } = this.viewed;
    const offset = page * limit - limit;

    await structFlow<Api.EditionPeriodsResponse>(
      this.viewedStruct,
      () => api.get('...', {
        params: { offset, limit },
        withCredentials: true,
      }),
      {
        transformResponse: ({ data }: Api.EditionPeriodsResponse) => ({
          data: data.map(
            ({ issues, ...rest }: Api.EditionPeriod) => ({
              issues: issues.map(addIssueUrl),
              ...rest,
            }),
          ),
        }),
      },
    );

    if (_.get(this.viewedStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }

    return this.viewedStruct;
  };

  @action.bound
  public fetchViewedIfNeeded = async () => {
    await fetchPaginalData({
      fetchFunction: () => this.fetchViewed(),
      item: this.viewed,
    });
  }
}
