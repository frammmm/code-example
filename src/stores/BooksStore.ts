import { observable, action } from 'mobx';
import _ from 'lodash';
import api from 'api';
import { getDefaultStruct, structFlow } from 'utils/struct';
import { Api, Stores } from 'types';
import { fetchPaginalData, getPaginalItem } from '../utils/paginalItem';
import { addBookReviewItemUrl } from '../utils/urls';

export class BooksStore implements Stores.IBooksStore {
  private root: { [key: string]: any };
  @observable bookStruct = getDefaultStruct<Api.Book.Response>();
  @observable bookVoteStruct = getDefaultStruct<Api.Book.VoteResponse>();
  @observable reviewsPagesStruct = getDefaultStruct<Api.ReviewsResponse>();
  @observable reviewGenresStruct = getDefaultStruct<Api.BookGenre[]>();
  @observable reviewBookThemesStruct = getDefaultStruct<Api.BookTheme[]>();

  @observable public reviewsPages = getPaginalItem<Api.Review>();

  constructor(stores: any) {
    this.root = stores;
  }

  @action.bound
  clearStructs = () => {
    this.bookStruct = getDefaultStruct();
    this.bookVoteStruct = getDefaultStruct();
    this.reviewsPagesStruct = getDefaultStruct();
    this.reviewGenresStruct = getDefaultStruct();
    this.reviewBookThemesStruct = getDefaultStruct();
  }

  @action.bound
  fetchBook = async (id: string) => {
    await structFlow<Api.Book.Response>(
      this.bookStruct,
      () => api.get(`...`, {
        withCredentials: true,
      }),
    );

    if (_.get(this.bookStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };

  @action.bound
  rateBook = async (params: { bookId: string; vote: Api.Book.Vote; alreadyVoted: boolean }) => {
    const { bookId, vote, alreadyVoted } = params;
    const method = alreadyVoted ? 'patch' : 'post';

    await structFlow<Api.Book.VoteResponse>(
      this.bookVoteStruct,
      () => api[method](`...`,
        { vote },
        {
          withCredentials: true,
        }),
    );

    if (_.get(this.bookVoteStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };


  @action.bound
  fetchReviewsPages = async (params?: object) => {
    const { page, limit } = this.reviewsPages;
    const offset = page * limit - limit;

    await structFlow<Api.ReviewsResponse>(
      this.reviewsPagesStruct,
      () => api.get('...', {
        params: { offset, limit, ...params },
        withCredentials: true,
      }),
      {
        transformResponse: ({ reviews }: any) => ({
          ...reviews,
          data: reviews.data.map(addBookReviewItemUrl),
        }),
      },
    );

    if (_.get(this.reviewsPagesStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }

    return this.reviewsPagesStruct;
  };

  @action.bound
  fetchReviewsPagesIfNeeded = async (params?: object) => {
    await fetchPaginalData({
      fetchFunction: () => this.fetchReviewsPages(params),
      item: this.reviewsPages,
    });
  }

  @action.bound
  fetchBookGenres = async () => {
    await structFlow<Api.BookGenre[]>(
      this.reviewGenresStruct,
      () => api.get('...', {
        withCredentials: true,
      }),
      {
        transformResponse: (data: Api.BookGenre[]) => data,
      },
    );

    if (_.get(this.reviewGenresStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };

  @action.bound
  fetchBookThemes = async () => {
    await structFlow<Api.BookTheme[]>(
      this.reviewBookThemesStruct,
      () => api.get('...', {
        withCredentials: true,
      }),
      {
        transformResponse: (data: Api.BookTheme[]) => data,
      },
    );

    if (_.get(this.reviewBookThemesStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };
}
