import { observable, action, autorun } from 'mobx';
import api from 'api';
import { Stores } from 'types';
import { getDefaultStruct, structFlow } from 'utils/struct';

export class AuthStore implements Stores.IAuthStore {
  private root: { [key: string]: any };
  @observable public isLoggedIn = false;
  @observable public temporaryEmail?: string;
  @observable public loginEmailStruct = getDefaultStruct<any>();
  @observable public loginCodeStruct = getDefaultStruct<any>();
  @observable public logoutStruct = getDefaultStruct<any>();

  public constructor(stores: any) {
    this.root = stores;
    this.initRehydration();
  }

  public initRehydration = () => {
    this.isLoggedIn = Boolean(localStorage.getItem('isLoggedIn'));
    autorun(() => {
      if (this.isLoggedIn) {
        localStorage.setItem('isLoggedIn', 'true');
      } else {
        localStorage.removeItem('isLoggedIn');
      }
    });
  };

  @action.bound
  public submitEmail = async ({ email }: { email: string }) => {
    this.loginCodeStruct = getDefaultStruct<any>();
    await structFlow<any>(this.loginEmailStruct, () => api.post('...', { email }));

    if (this.loginEmailStruct.data) {
      this.temporaryEmail = email;
    }
  };

  @action.bound
  public submitCode = async ({ code, email }: any) => {
    await structFlow<any>(this.loginCodeStruct, () => api.post(
      '...',
      { code, email, acceptAgreement: '1' },
      { withCredentials: true },
    ));

    if (this.loginCodeStruct.data) {
      this.temporaryEmail = undefined;
      this.isLoggedIn = true;
    }
  };

  @action.bound
  public logout = async () => {
    await structFlow<any>(this.logoutStruct, () => api.post(
      '...',
      null,
      { withCredentials: true },
    ));

    if (this.logoutStruct.data) {
      this.isLoggedIn = false;
    }
  };
}
