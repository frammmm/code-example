import { observable, action } from 'mobx';
import _ from 'lodash';
import api from 'api';
import { getDefaultStruct, structFlow } from 'utils/struct';
import { Api, Stores } from 'types';

export class ClientStore implements Stores.IClientStore {
  private root: { [key: string]: any };
  @observable public clientInfoStruct = getDefaultStruct<Api.ClientInfo.Response>();

  public constructor(stores: any) {
    this.root = stores;
  }

  @action.bound
  public fetchClientInfo = async () => {
    await structFlow<Api.ClientInfo.Response>(
      this.clientInfoStruct,
      () => api.get('...', {
        withCredentials: true,
      }),
    );

    if (_.get(this.clientInfoStruct.errors, 'status') === 401) {
      this.root.authStore.isLoggedIn = false;
    }
  };
}
