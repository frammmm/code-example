import * as React from 'react';
import Skeleton from 'react-loading-skeleton';
import cn from 'classnames/bind';

import { Breadcrumbs } from '../Breadcrumbs';
import { ContentWrapper } from '../ContentWrapper';
import { Title } from '../Title';

import styles from './index.module.scss';

const cx = cn.bind(styles);

export const IssueTextLoader = () => (
  <>
    <Breadcrumbs>
      <Skeleton width={300} height={18} />
    </Breadcrumbs>
    <ContentWrapper className={cx('content-inner')}>
      <div className={cx('left-container')}>

      </div>
      <div className={cx('center-container')}>
        <Title as="h2">
          <Skeleton height={40} width={'80%'} />
        </Title>
        <div className={cx('subtitle-container')}>
          <Skeleton width={90} />
        </div>
        <Skeleton count={12} />
      </div>
      <div className={cx('right-container')}>

      </div>
    </ContentWrapper>
  </>
);
