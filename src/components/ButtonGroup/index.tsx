import * as React from 'react';
import cn from 'classnames/bind';
import styles from './index.module.scss';

const cx = cn.bind(styles);

export interface IProps {
  children: any;
  className?: string;
  inline?: boolean;
  space?: boolean;
}

export class ButtonGroup extends React.Component<IProps> {
  render() {
    const {
      children, className, inline = false, space = false,
    } = this.props;
    const offsetClass = inline ? 'offset-right' : 'offset-bottom';
    return (
      <div className={cx('button-group', offsetClass, className, { space, inline })}>
        {children}
      </div>
    );
  }
}
