import * as _ from 'lodash';
import cn from 'classnames/bind';
import React from 'react';
import { Link } from 'react-router-dom';

import { useTheme } from 'contexts';
import 'rc-dropdown/assets/index.css';
import styles from './index.module.scss';

const cx = cn.bind(styles);

// eslint-disable-next-line global-require
export const DropdownMenu = require('rc-dropdown').default as any;

const DropdownList = ({
  className,
  ...rest
}: React.HTMLAttributes<HTMLUListElement>) => (
  <ul className={cx('dropdown-menu', className)} {..._.omit(rest, 'prefixCls')} />
);

const DropdownListItem = ({ className, ...rest }: React.HTMLAttributes<HTMLLIElement>) => (
  <li className={cx('item', className)} {...rest}/>
);

const DropdownSeparator = ({ className, ...rest }: React.HTMLAttributes<HTMLLIElement>) => (
  <li className={cx('separator', className)} {...rest}/>
);

const DropdownToggler = ({
  className, children, ...rest
}: React.HTMLAttributes<HTMLSpanElement>) => {
  const [{ primaryColor }] = useTheme();
  const style = { color: primaryColor };
  return (
    <span className={cx('toggler', className)} style={style} {...rest}>{children}</span>
  );
};


interface IDropdownLinkProps {
  as?: 'span' | 'a' | 'link';
  [key: string]: any;
}

const DropdownLink = ({ className, as = 'link', ...rest }: IDropdownLinkProps) => {
  if (as === 'span') {
    return <span className={cx('link', className)} {...rest}/>;
  }
  if (as === 'a') {
    return <a className={cx('link', className)} {...rest}>{rest.children}</a>;
  }
  return <Link className={cx('link', className)} {...rest as any}/>;
};

export const Dropdown = {
  List: DropdownList,
  ListItem: DropdownListItem,
  Separator: DropdownSeparator,
  Link: DropdownLink,
  Toggler: DropdownToggler,
};
