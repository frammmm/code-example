import * as React from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames/bind';
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

import { Api } from 'types';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps extends React.HTMLAttributes<HTMLDivElement> {
  item: Api.Issue;
  renderDate?: boolean;
  responsiveMobile?: boolean;
}

const IssueCard = ({
  className,
  item: {
    url, edition, cover, id, date,
  },
  renderDate,
  responsiveMobile = false,
}: IProps) => (
  <Link
    to={url}
    className={cx('card', className, {
      'responsive-mobile': responsiveMobile,
    })}
    key={id}
    data-test="issue-card-link"
  >
    <img className={cx('image', { 'responsive-mobile': responsiveMobile })} src={cover} alt="preview" />
    {edition.name && <span className={cx('title')}>{edition.name}</span>}
    {renderDate && <span className={cx('title')}>{format(new Date(date), 'd MMMM', { locale: ru })}</span>}
  </Link>
);

export { IssueCard };
