import * as React from 'react';
import Skeleton from 'react-loading-skeleton';
import * as _ from 'lodash';
import cn from 'classnames/bind';

import styles from './index.module.scss';

const cx = cn.bind(styles);

export const BookReviewFormLoader = () => (
  <div className={cx('left-container', 'is-hidden-xs')}>
    {_.times(2).map((index) => (
      <div key={index} className={cx('filters-group')}>
        <div className={cx('filters-group-header')}>
          <Skeleton height={20} width={75} />
        </div>
        <div className={cx('filter')}>
          <Skeleton height={24} />
        </div>
        <div className={cx('filter')}>
          <Skeleton height={24} />
        </div>
        <div className={cx('filter')}>
          <Skeleton height={24} />
        </div>
        <div className={cx('filter')}>
          <Skeleton height={24} />
        </div>
      </div>
    ))}
  </div>
);
