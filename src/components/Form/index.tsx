import React from 'react';
import * as _ from 'lodash';

interface IProps extends React.InputHTMLAttributes<HTMLFormElement> {
  children?: React.ReactNode;
  onSubmit?(p: any): void;
}

class Form extends React.Component<IProps> {
  static defaultProps = {
    onSubmit: () => {},
  }

  private handleSubmit = (e: any) => {
    e.preventDefault();

    // Get checkboxes names from DOM due to lack of it in formData
    const formElements = e.target.elements;
    const formElementsArray: any[] = [];

    _.forEach(formElements, (el) => { formElementsArray.push(el); });

    const formPairs = formElementsArray
      .map(({ name, type, value }: any) => {
        const newValue = type === 'checkbox' ? Boolean(value) : value;
        return [name, newValue];
      });
    const formValues = _.fromPairs(formPairs);

    this.props!.onSubmit!(formValues);
  }

  render() {
    const { className, children } = this.props;
    return (
      <form
        className={className}
        onSubmit={this.handleSubmit}>
        {children}
      </form>
    );
  }
}

export { Form };
