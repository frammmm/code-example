import * as React from 'react';
import cn from 'classnames/bind';

import { ImageLoader } from '../ImageLoader';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  pageNum: number;
  imageUrl: string;
  mobile?: boolean;
  onClickRegion(p: { x: number; y: number; pageNum: number }): void;
}

export class ImageWithCoordNav extends React.Component<IProps> {
  imageRef = React.createRef<HTMLImageElement>();

  private handleClickImage = (e: any) => {
    const { clientX, clientY } = e;
    const { current } = this.imageRef;

    if (current) {
      const coords = current.getBoundingClientRect();
      const x = ((clientX - coords.left) / coords.width) * 100;
      const y = ((clientY - coords.top) / coords.height) * 100;

      const { onClickRegion, pageNum } = this.props;
      if (onClickRegion) {
        onClickRegion({ x, y, pageNum });
      }
    }
  }

  render() {
    const { imageUrl, mobile = false } = this.props;
    const image = (
      <ImageLoader
        alt=""
        src={imageUrl}
        key={imageUrl}
        className={cx('image-spread')}
        imageClassName={cx('image')}
        loadingClassName="is-full-width"
        onClick={this.handleClickImage}
        ref={this.imageRef}
      />
    );
    if (mobile) { return image; }
    return (
      <div className={cx('image-spread-container')}>
        {image}
      </div>
    );
  }
}
