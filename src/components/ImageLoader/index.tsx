import React, { forwardRef, useState } from 'react';
import cn from 'classnames/bind';

import { Spinner } from '../Spinner';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps extends React.ImgHTMLAttributes<HTMLImageElement> {
  aspectRatio?: number;
  forwardedRef?: any;
  imageClassName?: string;
  loadingClassName?: string;
}

const ImageLoaderComponent = (props: IProps) => {
  const [isLoading, setLoading] = useState(true);

  const {
    alt,
    className,
    forwardedRef,
    imageClassName,
    loadingClassName,
    onLoad,
    src,
    ...rest
  } = props;

  const handleLoad = (event: any) => {
    setLoading(false);
    if (onLoad) { onLoad(event); }
  };

  if (!src) { return null; }

  return (
    <div className={cx('image-loader', className, {
      [`${loadingClassName}`]: loadingClassName && isLoading,
    })}>
      {isLoading && <Spinner color="white" ignoreContent />}
      <img
        alt={alt}
        src={src}
        className={cx('image', imageClassName, { 'is-visually-hidden': isLoading })}
        onLoad={handleLoad}
        ref={forwardedRef}
        onError={console.error}
        {...rest}
      />
    </div>
  );
};

const ImageLoader = forwardRef((props: IProps & { ref: any }, ref?: any) => (
  <ImageLoaderComponent {...props} forwardedRef={ref} />
));

ImageLoader.displayName = 'ImageLoader';

export { ImageLoader };
