import * as React from 'react';
import * as _ from 'lodash';
import cn from 'classnames/bind';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps extends React.HTMLAttributes<HTMLDivElement> {
  children?: any;
  isLoading?: boolean;
}

export class CardsCarousel extends React.Component<IProps> {
  state = {
    clientWidth: undefined,
    scrollWidth: undefined,
  }

  private contentRef = React.createRef<HTMLDivElement>()

  componentDidMount() {
    this.updateHeightValues();
    window.addEventListener('resize', this.onResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  private onResize = _.debounce(() => this.updateHeightValues(), 300)

  private updateHeightValues = () => {
    if (this.contentRef.current) {
      const { clientWidth, scrollWidth } = this.contentRef.current;

      if (
        this.state.clientWidth !== clientWidth
        || this.state.scrollWidth !== scrollWidth
      ) {
        this.setState({ clientWidth, scrollWidth });
      }
    }
  }

  private scrollForward = () => {
    const { current } = this.contentRef;

    if (current) {
      const { clientWidth, scrollWidth } = current;
      const nextScrollWidth = Math.min(clientWidth + current.scrollLeft, scrollWidth);
      current.scrollLeft = nextScrollWidth;
    }
  }

  render() {
    const {
      props: { children, isLoading },
      state: { clientWidth, scrollWidth },
    } = this;
    const hasScroll = clientWidth !== scrollWidth;

    return (
      <div className={cx('carousel', { 'is-loading': isLoading })}>
        <div className={cx('block-parent-flex-width')}>
          <div ref={this.contentRef} className={cx('cards-container')}>
            {children}
          </div>
          {!isLoading && hasScroll && (
            <div onClick={this.scrollForward} className={cx('control-right')} />
          )}
        </div>
      </div>
    );
  }
}
