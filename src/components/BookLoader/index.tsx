import * as React from 'react';
import Skeleton from 'react-loading-skeleton';
import cn from 'classnames/bind';
import Media from 'react-responsive';
import { Breakpoints } from 'utils/constants';
import { ContentWrapper } from '../ContentWrapper';
import styles from './index.module.scss';
import { Breadcrumbs } from '../Breadcrumbs';
import { Title } from '../Title';

const cx = cn.bind(styles);

const mobileLoader = () => (
  <>
    <ContentWrapper className={cx('top-container')}>
      <div className={cx('cover-container')}>
        <Skeleton />
      </div>
      <div className={cx('title')}>
        <Skeleton height={22} count={2} />
      </div>
      <div className={cx('author')}>
        <Skeleton width={'80%'} height={18} />
      </div>
      <Skeleton width={'80%'} height={18} />
    </ContentWrapper>
  </>
);

const desktopLoader = () => (
  <>
    <Breadcrumbs>
      <Skeleton width={200} height={18} />
    </Breadcrumbs>
    <ContentWrapper className={cx('content-inner')}>
      <div className={cx('left-container')}>
        <div className={cx('cover-container')}>
          <Skeleton width={318} height={408} />
        </div>
      </div>
      <div className={cx('right-container')}>
        <Title as="h2" className={cx('title')}>
          <Skeleton width={'80%'} />
        </Title>
        <div style={{ marginBottom: 10 }}>
          <Skeleton width={300} height={40} />
        </div>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Skeleton height={16} width={200} />
          <Skeleton height={16} width={200} />
        </div>
        <div className={cx('section')}>
          <div className={cx('section-title')}>
            <Skeleton width={100} />
          </div>
          <Skeleton count={5} />
        </div>
      </div>
    </ContentWrapper>
  </>
);

const BookLoader = () => (
  <div className={cx('container')}>
    <div className={cx('content')}>
      <Media maxDeviceWidth={Breakpoints.sm}>
        {(matches) => (matches ? mobileLoader() : desktopLoader())}
      </Media>
    </div>
  </div>
);

export { BookLoader };
