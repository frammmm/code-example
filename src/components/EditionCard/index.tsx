import * as React from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames/bind';

import { Api } from 'types';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps extends React.HTMLAttributes<HTMLDivElement> {
  item: Api.Edition;
  responsiveMobile?: boolean;
  shadows?: boolean;
}

export const EditionCard = ({
  item: {
    url, id, name, latestIssue,
  },
  responsiveMobile,
  shadows = true,
}: IProps) => (
  <Link
    to={url}
    key={id}
    className={cx('card', {
      'responsive-mobile': responsiveMobile,
      'shadow': shadows,
    })}
    data-test="edition-card-link"
  >
    <div className={cx('image-container')}>
      <img className={cx('image')} src={latestIssue && latestIssue.cover} alt={name} />
    </div>
    <span className={cx('title')}>{name}</span>
  </Link>
);
