import * as React from 'react';
import cn from 'classnames/bind';

import { Title, TitleElements } from 'components/Title';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  title: any;
  titleAs?: TitleElements;
  titleClassName?: string;
  subtitle?: any;
  link?: any;
  gradient?: boolean;
  separator?: boolean;
  topPadding?: 'medium' | boolean | undefined;
  verticalPaddings?: 'medium' | boolean | undefined;
  children: React.ReactNode;
}

const Section = ({
  title,
  titleAs,
  titleClassName,
  subtitle,
  link,
  gradient = false,
  separator = false,
  children,
  topPadding,
  verticalPaddings,
}: IProps) => (
  <div className={cx('section', { gradient })}>
    <div
      className={cx('section-content', {
        separator,
        [`padding-top-${topPadding}`]: topPadding,
        [`padding-${verticalPaddings}`]: verticalPaddings,
      })}
    >
      <div className={cx('head-container')}>
        <div className={cx('title-container')}>
          <Title as={titleAs || 'h4'} className={cx('title', titleClassName)}>
            {title}
          </Title>
          {subtitle && <span>{subtitle}</span>}
        </div>
        {link && <div className={cx('link-container')}>{link}</div>}
      </div>
      {children}
    </div>
  </div>
);

export { Section };
