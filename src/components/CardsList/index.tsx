import * as React from 'react';
import * as _ from 'lodash';
import cn from 'classnames/bind';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps extends React.HTMLAttributes<HTMLDivElement> {
  children?: any;
  className?: string;
  maxItemsPerRow?: number;
  mobileGallery?: boolean;
  mobileThreeColumnLayout?: boolean;
}

const CardsContainer = ({
  children,
  className,
  maxItemsPerRow,
  mobileGallery,
  mobileThreeColumnLayout,
}: IProps) => {
  let nextChildren = children;

  if (maxItemsPerRow) {
    // eslint-disable-next-line
    // @ts-ignore
    const itemsToAppend = maxItemsPerRow - (children.length % maxItemsPerRow);

    if (itemsToAppend && children[0]) {
      _.times(itemsToAppend).forEach((index) => {
        nextChildren.push(React.cloneElement(children[0], {
          key: -index - 1,
          className: 'empty',
        }));
      });
    }

    nextChildren = children;
  }

  return (
    <div
      className={cx('cards', className, {
        'responsive': maxItemsPerRow,
        'mobile-gallery': mobileGallery,
        'mobile-three-columns': mobileThreeColumnLayout,
      })}
    >
      {nextChildren}
    </div>
  );
};

const CardsListItem = ({
  className,
  ...rest
}: React.HTMLAttributes<HTMLDivElement>) => (
  <div className={cx('card', className)} {...rest} />
);

export const CardsList = {
  Container: CardsContainer,
  Item: CardsListItem,
};
