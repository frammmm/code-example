import * as React from 'react';
import Skeleton from 'react-loading-skeleton';
import cn from 'classnames/bind';
import * as _ from 'lodash';

import { Section } from '../Section';
import { CardsList } from '../CardsList';
import { ContentWrapper } from '../ContentWrapper';
import { Breadcrumbs } from '../Breadcrumbs';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  breadcrumbs?: boolean;
}

export const SectionLoader = ({ breadcrumbs = false }: IProps) => (
  <>
    {breadcrumbs && (
      <Breadcrumbs>
        <Skeleton width={180} height={18}/>
      </Breadcrumbs>
    )}
    <Section title={<Skeleton width={200} />}>
      <ContentWrapper>
        <CardsList.Container>
          {_.times(4, (idx) => (
            <CardsList.Item key={idx}>
              <div className={cx('card')}>
                <div className='is-hidden-xs'>
                  <Skeleton width={196} height={283} />
                  <div style={{ marginTop: 10 }}>
                    <Skeleton width={60} height={18}/>
                  </div>
                </div>
                <div className='is-visible-xs'>
                  <Skeleton width={'100%'} height={190} />
                  <div style={{ marginTop: 10 }}>
                    <Skeleton width={60} height={18}/>
                  </div>
                </div>
              </div>
            </CardsList.Item>
          ))}
        </CardsList.Container>
      </ContentWrapper>
    </Section>
  </>
);
