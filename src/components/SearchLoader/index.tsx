import * as React from 'react';
import * as _ from 'lodash';
import Skeleton from 'react-loading-skeleton';
import cn from 'classnames/bind';

import { CardsCarousel } from '../CardsCarousel';
import { ContentWrapper } from '../ContentWrapper';
import { Title } from '../Title';

import styles from './index.module.scss';

const cx = cn.bind(styles);

export const SearchLoader = () => (
  <>
    <ContentWrapper>
      <Title as="h2" className={cx('title')}>
        <Skeleton height={32} width={100} />
      </Title>
      <ContentWrapper wide>
        <ContentWrapper>
          <Title as="h5" className={cx('subtitle')}>
            <Skeleton height={20} width={200} />
          </Title>
        </ContentWrapper>
        <div className={cx('slider-container')}>
          <CardsCarousel isLoading>
            {_.times(5).map((index) => (
              <div key={index} className={cx('card')}>
                <div className='is-hidden-xs'>
                  <Skeleton width={196} height={283} />
                  <div style={{ marginTop: 10 }}>
                    <Skeleton width={60} height={18} />
                  </div>
                </div>
                <div className='is-visible-xs'>
                  <Skeleton width={150} height={215} />
                  <div style={{ marginTop: 10 }}>
                    <Skeleton width={60} height={18} />
                  </div>
                </div>
              </div>
            ))}
          </CardsCarousel>
        </div>
      </ContentWrapper>
      <div className={cx('document-amount')}>
        <Skeleton width={185} height={24} />
      </div>
      {_.times(3).map((index) => (
        <div className={cx('document')} key={index}>
          <div className={cx('document-title')}>
            <Skeleton width={160} height={17} />
          </div>
          <div className={cx('document-issue')}>
            <Skeleton width={80} height={16} />
          </div>
          <div className={cx('document-description')}>
            <Skeleton width={'75%'} height={24} />

          </div>
        </div>
      ))}
    </ContentWrapper>
  </>
);
