import * as React from 'react';
import cn from 'classnames/bind';
import { useTheme } from 'contexts';
import { Spinner } from '../Spinner';
import styles from './index.module.scss';

const cx = cn.bind(styles);

type Color = 'primary' | 'secondary' | 'white' | 'transparent';

export interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  color?: Color;
  fullwidth?: boolean;
  loading?: boolean;
  asLink?: boolean;
  round?: 'md' | 'xl';
}

const Button = ({
  children,
  className,
  fullwidth = false,
  color = 'primary',
  loading = false,
  round,
  style = {},
  asLink = false,
  ...props
}: IButtonProps) => {
  const [theme] = useTheme();
  const shouldUseTheme = color === 'primary';
  let finalStyle = style;
  if (color === 'primary') {
    finalStyle = {
      ...style,
      backgroundColor: theme.primaryColor,
      color: theme.primaryContrastingColor,
    };
  }
  if (asLink) {
    finalStyle = {
      ...style,
      color: theme.primaryColor,
    };
  }
  const finalClassName = cx(
    'btn',
    className,
    {
      fullwidth,
      loading,
      'as-link': asLink,
      [color]: shouldUseTheme ? false : color,
      [`round-${round}`]: round,
    },
  );
  return (
    <button
      className={finalClassName}
      style={finalStyle}
      type="submit"
      {...props}
    >
      {loading ? (
        <Spinner color={color === 'white' ? 'primary' : 'white'}/>
      )
        : children
      }
    </button>
  );
};

export { Button };
