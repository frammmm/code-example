export type TIconNames =
  | 'aA'
  | 'checkmark-white'
  | 'chevron-left-gray'
  | 'chevron-right-gray'
  | 'gamburger'
  | 'newspaper'
  | 'lines'
  | 'play-button'
  | 'times'
  | 'times-black-sm'
  | 'times-blue-sm';
