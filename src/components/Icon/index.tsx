import React from 'react';
import cn from 'classnames/bind';

import { TIconNames } from './icons';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps extends React.HTMLAttributes<HTMLSpanElement> {
  name: TIconNames;
  noPointerEvents?: boolean;
  onClick?(event: React.MouseEvent): void;
}

const Icon = (props: IProps) => {
  const {
    className, name, noPointerEvents, ...rest
  } = props;

  const classes = cx(
    'icon',
    className,
    name,
  );

  if (noPointerEvents) {
    rest.style = {
      ...rest.style,
      pointerEvents: 'none',
    };
  }

  return (
    <span className={classes} {...rest} />
  );
};

export { Icon };
