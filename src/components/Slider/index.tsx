import * as React from 'react';
import RCSlider, { SliderProps } from 'rc-slider';
import { Helmet } from 'react-helmet';

import { useTheme } from 'contexts';
import 'rc-slider/assets/index.css';
import './index.scss';

export const Slider = (props: SliderProps) => {
  const [{ primaryColor }] = useTheme();
  return (
    <>
      <Helmet>
        <style>
          {`
            .rc-slider-handle {
              background-color: ${primaryColor};
            }
          `}
        </style>
      </Helmet>
      <RCSlider {...props} />
    </>
  );
};
