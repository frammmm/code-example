import * as React from 'react';
import cn from 'classnames/bind';
import styles from './index.module.scss';

const cx = cn.bind(styles);

export const Breadcrumbs = ({ children }: React.HTMLAttributes<HTMLDivElement>) => (
  <div className={cx('breadcrumbs')}>
    <div className={cx('inner')}>
      {children}
    </div>
  </div>
);
