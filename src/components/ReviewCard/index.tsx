import * as React from 'react';
import Skeleton from 'react-loading-skeleton';
import cn from 'classnames/bind';

import { Api } from 'types';
import { ButtonLink } from '../ButtonLink';
import { Icon } from '../Icon';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps extends React.HTMLAttributes<HTMLDivElement> {
  item: Api.Review;
}

export const ReviewCard = ({ item }: IProps) => (
  <div className={cx('card')}>
    <div className={cx('cover-container')}>
      <img className={cx('cover')} src={item.book.cover} alt={item.book.title} />
    </div>
    <div className={cx('image-container')}>
      <img className={cx('image')} src={item.cover} alt="" />
      <div className={cx('play-button')}>
        <Icon name="play-button" />
      </div>
    </div>
    <span className={cx('title')}>{item.book.title}</span>
    <span className={cx('author')}>Автор: {item.book.author}</span>
    <ButtonLink to={item.url} className={cx('link')}>Перейти к книге</ButtonLink>
  </div>
);

export const ReviewCardLoader = () => (
  <div className={cx('card')}>
    <Skeleton width={'100%'} height={140} />
    <div style={{ marginTop: 6, marginBottom: 3 }}>
      <Skeleton />
      <Skeleton width={'80%'} />
    </div>
    <Skeleton height={32} />
  </div>
);
