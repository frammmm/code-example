import React from 'react';
import cn from 'classnames/bind';
import { Field, FieldProps } from 'formik';

import styles from './index.module.scss';

const cx = cn.bind(styles);

export const Control = ({
  children, label, id, error, className,
}: any) => (
  <div className={cx(className, 'form-control', { 'form-error': error })}>
    {label && (
      <label htmlFor={id} className={cx('label')}>
        {label}
      </label>
    )}
    {children}
  </div>
);

interface ITextProps extends React.InputHTMLAttributes<HTMLInputElement> {
  color?: 'gray';
  error?: any;
  label?: string;
}

const Text = React.forwardRef(
  (
    {
      onChange,
      value,
      defaultValue,
      type = 'string',
      label,
      error,
      color,
      className,
      ...rest
    }: ITextProps,
    ref: React.Ref<HTMLInputElement>,
  ) => {
    const inputProps = onChange
      ? {
        onChange,
        value,
        type,
        ...rest,
      }
      : {
        defaultValue,
        type,
        ...rest,
      };
    return (
      <Control label={label} id={rest.id} error={error}>
        <input ref={ref} className={cx('input-text', className, { [color as string]: color })} {...inputProps} />
        {error && <p className={cx('errors-text')}> {error}</p>}
      </Control>
    );
  },
);

interface ICheckboxProps extends React.InputHTMLAttributes<HTMLInputElement> {
  controlClassName?: string;
  color?: 'primary';
  error?: any;
  label?: string | React.ReactNode;
  labelClassName?: string;
  reverse?: boolean;
  spread?: boolean;
}

const Checkbox = ({
  onChange, controlClassName, color, error, label, labelClassName, reverse, spread, ...rest
}: ICheckboxProps) => {
  const inputProps = onChange ? { onChange, ...rest } : rest;
  return (
    <Control className={controlClassName} error={error}>
      <div className={cx('checkbox-container', { reverse, spread })}>
        <input
          className={cx('input-checkbox', color)}
          type="checkbox"
          {...inputProps}
        />
        <label htmlFor={rest.id} className={cx('label-checkbox', labelClassName)}>
          {label}
        </label>
      </div>
      {error && <p className={cx('errors-text')}> {error}</p>}
    </Control>
  );
};

interface IFormikCheckboxProps extends ICheckboxProps {
  id?: string;
  name: string;
  value?: any;
  onChange?(): void;
}

const FormikCheckbox = ({
  name,
  onChange,
  value,
  ...rest
}: IFormikCheckboxProps) => (
  <Field name={name}>
    {({ field, form }: FieldProps) => {
      const checked = Array.isArray(field.value)
        ? field.value.includes(value)
        : value === field.value;

      return (
        <Checkbox
          checked={checked}
          value={value}
          onChange={() => {
            if (field.value.includes(value)) {
              form.setFieldValue(name, field.value.filter((v: any) => v !== value));
            } else {
              form.setFieldValue(name, field.value.concat(value));
            }

            if (onChange) { onChange(); }
          }}
          {...rest}
        />
      );
    }}
  </Field>
);

export const FormControl = {
  Checkbox,
  FormikCheckbox,
  Text,
};
