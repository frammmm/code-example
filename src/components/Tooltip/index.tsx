import React from 'react';
import Tippy, { TippyProps } from '@tippy.js/react';
import cn from 'classnames/bind';

import './index.scss';
import styles from './index.module.scss';

const cx = cn.bind(styles);

export const Tooltip = ({
  children, className, content, ...props
}: TippyProps) => (
  <Tippy
    arrow
    duration={[275, 0]}
    className={cx('tooltip', className)}
    interactive
    popperOptions={{ positionFixed: true }}
    theme='white'
    content={(<div className={cx('content')}>{content}</div>)}
    {...props}
  >
    {children}
  </Tippy>
);
