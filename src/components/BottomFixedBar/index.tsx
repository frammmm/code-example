import * as React from 'react';
import cn from 'classnames/bind';
import { ContentWrapper } from 'components/ContentWrapper';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  children?: React.ReactNode;
  className?: string;
}

export const BottomFixedBar = ({ children, className }: IProps) => (
  <div className={cx('container', className)}>
    <ContentWrapper className={cx('content')}>
      {children}
    </ContentWrapper>
  </div>
);
