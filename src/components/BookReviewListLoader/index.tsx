import * as React from 'react';
import Skeleton from 'react-loading-skeleton';
import * as _ from 'lodash';
import cn from 'classnames/bind';

import { CardsList } from '../CardsList';
import { ReviewCardLoader } from '../ReviewCard';
import styles from './index.module.scss';

const cx = cn.bind(styles);

export const BookReviewListLoader = () => (
  <CardsList.Container className={cx('cards')} maxItemsPerRow={8}>
    {_.times(4).map((index) => (
      <CardsList.Item key={index} style={{ maxWidth: 'none', padding: 0, margin: '15px 8px' }}>
        <ReviewCardLoader />
      </CardsList.Item>
    ))}
  </CardsList.Container>
);

export const BookReviewLoader = () => (
  <div className={cx('right-container')}>
    <div className="is-hidden-xs" style={{ marginBottom: 16 }}>
      <Skeleton width={205} height={36} />
    </div>
    <div className={cx('search-input')} style={{ marginBottom: 32 }}>
      <Skeleton height={36} width={'100%'} />
    </div>
    <BookReviewListLoader />
  </div>
);
