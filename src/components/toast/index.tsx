import React from 'react';
import { toast as reactToast, Flip } from 'react-toastify';
import cn from 'classnames/bind';

import 'react-toastify/dist/ReactToastify.css';
import './index.scss';
import styles from './index.module.scss';

const cx = cn.bind(styles);

reactToast.configure({
  position: 'top-right',
  autoClose: 5000,
  hideProgressBar: false,
  newestOnTop: true,
  closeOnClick: true,
  rtl: false,
  draggable: true,
  pauseOnHover: true,
  transition: Flip,
  closeButton: false,
});

type TooltipTypes = 'info' | 'error';

interface IProps {
  children: React.ReactNode;
  kind: TooltipTypes;
}

const ToastBody = ({ children }: IProps) => (
  <div className={cx('body')}>
    <div className={cx('content')}>
      {children}
    </div>
  </div>
);

const customToast = (kind: TooltipTypes) => (text: string, config?: any) => reactToast(
  <ToastBody kind={kind}>{text}</ToastBody>,
  {
    className: cx('container', kind),
    progressClassName: cx('progress', `progress-${kind}`),
    ...config,
  },
);

export const toast = {
  info: (...args: [any, any?]) => customToast('info')(...args),
  error: (...args: [any, any?]) => customToast('error')(...args),
};
