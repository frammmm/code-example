import * as React from 'react';
import { withRouter, RouteComponentProps } from 'react-router';
import { Button, IButtonProps } from '../Button';

interface IProps extends IButtonProps, RouteComponentProps {
  to: string;
}

export const ButtonLink = withRouter(({
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  history, to, staticContext, ...props
}: IProps) => (
  <Button
    onClick={() => { history.push(to); }}
    {...props}
  />
));
