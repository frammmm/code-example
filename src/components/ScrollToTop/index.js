import * as React from 'react';
import { withRouter } from 'react-router';

export const ScrollToTop = withRouter(({
  children,
  location: { pathname, search },
}) => {
  React.useEffect(() => {
    setTimeout(() => window.scrollTo(0, 0), 0);
  }, [pathname, search]);

  return children || null;
});
