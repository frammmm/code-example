import * as React from 'react';
import * as _ from 'lodash';
import cn from 'classnames/bind';

import styles from './index.module.scss';

const cx = cn.bind(styles);

export type TitleElements = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'h7' | 'h8';

interface IProps extends React.InputHTMLAttributes<HTMLHeadingElement> {
  as: TitleElements;
  tag?: string;
  center?: boolean;
}

class Title extends React.Component<IProps> {
  public static defaultProps = {
    as: 'h1',
    center: false,
  };

  private get elementName(): any {
    const headerlEments = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
    const { as } = this.props;
    return _.includes(headerlEments, as) ? as : 'p';
  }

  public render() {
    const {
      as, tag, children, className, center, ...props
    } = this.props;
    const ElementName = tag || this.elementName;
    const classes = cx(className, as, 'header', { center });

    return (
      <ElementName className={classes} {...props}>
        {children}
      </ElementName>
    );
  }
}

export { Title };
