import * as React from 'react';
import Media from 'react-responsive';
import Skeleton from 'react-loading-skeleton';
import * as _ from 'lodash';
import cn from 'classnames/bind';

import { Breakpoints } from 'utils/constants';
import { Breadcrumbs } from '../Breadcrumbs';
import { CardsList } from '../CardsList';
import { ContentWrapper } from '../ContentWrapper';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  breadcrumbs?: boolean;
  buttons?: boolean;
  description?: boolean;
  type?: 'preview' | 'text';
  wideCards?: boolean;
}

const EditionMobileLoader = ({
  breadcrumbs = true,
  buttons = true,
  description = true,
}: IProps) => (
  <>
    {breadcrumbs && (
      <Breadcrumbs>
        <Skeleton width={200} height={18} />
      </Breadcrumbs>
    )}
    <div className={cx('content-inner')}>
      <ContentWrapper>
        <div style={{ marginBottom: 10 }}>
          <Skeleton width={300} height={28} />
        </div>
        {description && (
          <div style={{ marginBottom: 15 }}>
            <Skeleton count={3} />
          </div>
        )}
        {buttons && (
          <div style={{ marginBottom: 15 }}>
            <Skeleton height={40} />
          </div>
        )}
        <div style={{ maxWidth: 360, marginBottom: 28 }}>
          <Skeleton width='100%' height={450} />
        </div>
      </ContentWrapper>
    </div>
  </>
);

const EditionDesktopLoader = ({
  breadcrumbs = true,
  buttons = true,
  description = true,
  type = 'preview',
  wideCards = false,
}: IProps) => {
  let content;

  if (type === 'text') {
    content = (
      <div style={{ marginTop: 30 }}>
        {_.times(3).map((index) => (
          <div className={cx('snippet')} key={index} >
            <div style={{ marginBottom: 8 }}>
              <Skeleton width={270} />
            </div>
            <div style={{ marginBottom: 8 }}>
              <Skeleton count={3} />
            </div>
            <div style={{ marginBottom: 24 }}>
              <Skeleton width={100} />
            </div>
            <div>
              <Skeleton width={170} />
            </div>
          </div>
        ))}
      </div>
    );
  } else {
    content = (
      <div className={cx('sections-container')} style={{ marginTop: wideCards ? 20 : '' }}>
        {_.times(wideCards ? 3 : 2).map((sectionIndex) => (
          <div className={cx('section')} key={sectionIndex}>
            {!wideCards && (<div className={cx('section-title')}>
              <div style={{ marginBottom: 10 }}>
                <Skeleton width={100} height={19} />
              </div>
            </div>)}
            <CardsList.Container mobileThreeColumnLayout>
              {_.times(wideCards ? 2 : 3).map((index) => (
                <CardsList.Item key={index}>
                  <Skeleton width={wideCards ? 340 : 196} height={wideCards ? 238 : 283} />
                </CardsList.Item>
              ))}
            </CardsList.Container>
          </div>
        ))}
      </div>
    );
  }

  return <>
    {breadcrumbs && (
      <Breadcrumbs>
        <Skeleton width={200} height={18} />
      </Breadcrumbs>
    )}
    <ContentWrapper className={cx('content-inner')}>
      <div className={cx('left-container')}>
        <div style={{ width: '100%', maxWidth: 318 }}>
          <Skeleton height={408} />
        </div>
      </div>
      <div className={cx('right-container')}>
        <div style={{ marginBottom: 10 }}>
          <Skeleton width={300} height={25} />
        </div>
        {description && (
          <div style={{ marginBottom: 15 }}>
            <Skeleton count={3} />
          </div>
        )}
        {buttons && (
          <div style={{ marginBottom: 10 }}>
            <Skeleton width={270} height={40} />
          </div>
        )}

        {content}
      </div>
    </ContentWrapper>
  </>;
};

export const EditionLoader = (props: IProps) => (
  <Media maxDeviceWidth={Breakpoints.sm}>
    {(matches) => (matches
      ? <EditionMobileLoader {...props} />
      : <EditionDesktopLoader {...props} />
    )}
  </Media>
);
