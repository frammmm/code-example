import * as React from 'react';
import cn from 'classnames/bind';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps extends React.HTMLAttributes<HTMLDivElement> {
  wide?: boolean;
}

const ContentWrapper = ({
  children,
  className,
  wide,
  ...rest
}: IProps) => (
  <div className={cx(className, { 'normal': !wide, wide })} {...rest}>
    {children}
  </div>
);

export { ContentWrapper };
