import * as React from 'react';
import cn from 'classnames/bind';
// eslint-disable-next-line global-require
const RLE = require('react-lines-ellipsis') as any;

import { Button } from '../Button';
import styles from './index.module.scss';

const cx = cn.bind(styles);

type State = boolean | undefined;

const LinesEllipsis = (props: any) => {
  const [isClamped, setClamped] = React.useState<State>(undefined);
  if (isClamped === false) {
    return props.text;
  }
  return (
    <>
      <RLE
        onReflow={({ clamped }: any) => { if (clamped) { setClamped(true); } }}
        {...props}
      />
      <Button
        onClick={() => { setClamped(false); }}
        className={cx('search-cancel')}
        asLink
      >
        Показать полностью
      </Button>
    </>
  );
};

export { LinesEllipsis };
