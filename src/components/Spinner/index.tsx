import * as React from 'react';
import cn from 'classnames/bind';

import { useTheme } from 'contexts';
import styles from './index.module.scss';

const cx = cn.bind(styles);

type color = 'primary' | 'white';

interface IProps {
  color?: color;
  ignoreContent?: boolean;
}

export const Spinner = ({ color = 'white', ignoreContent }: IProps) => {
  const [{ primaryColor }] = useTheme();
  const style = color === 'primary' ? { backgroundColor: primaryColor } : undefined;
  const cl = cx(
    'spinner',
    {
      'ignore-content': ignoreContent,
      'white': color === 'white',
    },
  );
  return (
    <div className={cl}>
      <div style={style} className={cx('bounce1')} />
      <div style={style} className={cx('bounce2')} />
      <div style={style} className={cx('bounce3')} />
    </div>
  );
};
