import React from 'react';
import { inject, observer } from 'mobx-react';
import * as _ from 'lodash';

import { Stores } from 'types';
import { mixpanel } from 'utils/mixpanel';

interface IProps {
  clientStore?: Stores.IClientStore;
  authStore?: Stores.IAuthStore;
}

@inject('clientStore', 'authStore')
@observer
export class ClientInfoHandler extends React.Component<IProps> {
  componentDidMount() {
    const { authStore, clientStore } = this.props;

    if (authStore!.isLoggedIn) {
      clientStore!.fetchClientInfo();
    }
  }

  initStatisticOnce = _.once((data: any) => {
    mixpanel.identify(data.userEmail);
    mixpanel.people.set(_.pick(data, ['clientId', 'clientName', 'locale']));
  })

  componentDidUpdate() {
    const { data } = this.props.clientStore!.clientInfoStruct;
    if (data) {
      this.initStatisticOnce(data);
    }
  }

  render() {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { data } = this.props.clientStore!.clientInfoStruct;
    return null;
  }
}
