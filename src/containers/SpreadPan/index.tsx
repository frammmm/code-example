import React from 'react';
import cn from 'classnames/bind';
import styles from './index.module.scss';

const cx = cn.bind(styles);

const dragImagePreview = new Image(0, 0);
dragImagePreview.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';

interface IProps {
  identity: number;
  scale: number;
  className?: string;
  children?: React.ReactNode;
  onChangeScale(p: number): void;
  minScale: number;
  maxScale: number;
}

interface IState {
  rect?: ClientRect | DOMRect;
}

export class SpreadPan extends React.Component<IProps, IState> {
  spreadRef = React.createRef<HTMLDivElement>()

  translateX = 0
  translateY = 0
  dragStartX = 0
  dragStartY = 0

  state = {
    rect: undefined,
  }

  componentDidMount() {
    window.addEventListener('drop', this.handleDrop);
  }

  componentDidUpdate(prevProps: IProps) {
    const isScaleChanged = this.props.scale !== prevProps.scale;
    if (isScaleChanged) {
      this.setTransform();
    }
    if (this.props.identity !== prevProps.identity) {
      this.translateX = 0;
      this.translateY = 0;
      this.dragStartX = 0;
      this.dragStartY = 0;
      this.setTransform();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('drop', this.handleDrop);
  }

  private setRect = () => {
    if (this.spreadRef.current) {
      this.setState({
        rect: this.spreadRef.current.getBoundingClientRect(),
      });
    }
  }

  private getTransformCSS = () => {
    const { scale } = this.props;
    return `matrix(${scale}, 0.00, 0.00, ${scale}, ${this.translateX}, ${this.translateY})`;
  }

  private setTransform = () => {
    this.spreadRef.current!.style.transform = this.getTransformCSS();
  }

  private normalizeScale = (scale: number) => {
    const { minScale, maxScale } = this.props;
    return Math.max(minScale, Math.min(maxScale, scale));
  }

  private limitTranslation = (transitionLength: number, realLength: number, scale: number) => {
    const maxTranslationLength = (realLength * (scale - 1)) / scale / 2.0;
    return transitionLength >= 0
      ? Math.min(transitionLength, maxTranslationLength)
      : Math.max(transitionLength, -maxTranslationLength);
  }

  private setNewPosiiton = (({ clientX, clientY }: any) => {
    const { current } = this.spreadRef;

    if (!current) {
      return;
    }

    const { rect } = this.state;
    const { dragStartX, dragStartY } = this;
    if (rect && dragStartX && dragStartY) {
      const nextTranslateX = this.translateX - dragStartX + clientX;
      const nextTranslateY = this.translateY - dragStartY + clientY;
      const { scale } = this.props;

      // eslint-disable-next-line
      // @ts-ignore
      this.translateX = this.limitTranslation(nextTranslateX, rect.width, scale);
      // eslint-disable-next-line
      // @ts-ignore
      this.translateY = this.limitTranslation(nextTranslateY, rect.height, scale);

      this.setTransform();

      this.dragStartX = clientX;
      this.dragStartY = clientY;
    }
  })

  private handleDrop = (event: Event) => {
    event.preventDefault();
  }

  private handleWheel = ((e: any) => {
    e.persist();
    const { current } = this.spreadRef;

    if (!current) {
      return;
    }

    const { onChangeScale, scale } = this.props;

    const rect = current.getBoundingClientRect();
    const delta = (e.deltaY < 0 ? 1.1 : 0.9);
    const nextScale = this.normalizeScale(scale * delta);
    const deltaTranslateX = (((rect.width + rect.left) / 2 - e.clientX)) * (nextScale - scale);
    const deltaTranslateY = (((rect.height + rect.top) / 2 - e.clientY)) * (nextScale - scale);
    const nextTranslateX = this.translateX + deltaTranslateX;
    const nextTranslateY = this.translateY + deltaTranslateY;

    this.translateX = this.limitTranslation(nextTranslateX, rect.width, nextScale);
    this.translateY = this.limitTranslation(nextTranslateY, rect.height, nextScale);

    this.setTransform();

    onChangeScale(nextScale);
  })

  private handleDragStart = (e: any) => {
    e.dataTransfer.setDragImage(dragImagePreview, 0, 0);

    this.setRect();

    this.dragStartX = e.clientX;
    this.dragStartY = e.clientY;
  }

  private handleDrag = (e: any) => {
    e.preventDefault();

    const { clientX, clientY } = e;

    window.requestAnimationFrame(() => this.setNewPosiiton({ clientX, clientY }));
  }

  render() {
    const {
      className, children,
    } = this.props;

    return (
      <div
        onWheel={this.handleWheel}
        onDragStart={this.handleDragStart}
        onDragOver={this.handleDrag}
        className={className}
      >
        <div
          ref={this.spreadRef}
          className={cx('images-container')}
        >
          {children}
        </div>
      </div>
    );
  }
}
