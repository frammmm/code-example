import React from 'react';
// import { NavLink } from 'react-router-dom';
import cn from 'classnames/bind';
import styles from './index.module.scss';
import { Header } from './Header';
import { Footer } from './Footer';

const cx = cn.bind(styles);

interface IProps {
  children?: React.ReactNode;
  layoutProps?: {
    showHeader?: boolean;
    showFooter?: boolean;
    bottomOffsetForIssue?: boolean;
  };
}

const defaultLayoutProps = {
  showHeader: true,
  showFooter: false,
  bottomOffsetForIssue: false,
};

export const HomeLayout = ({ children, layoutProps = {} }: IProps) => {
  const layout = { ...defaultLayoutProps, ...layoutProps };
  return (
    <div className={cx('container', { 'has-header': layout.showHeader })}>
      {layout.showHeader && <Header /> }
      <div className={cx('content')}>{children}</div>
      {layout.showFooter && <Footer /> }
      {layout.bottomOffsetForIssue && (
        <div className={cx('bottom-offset')} />
      )}
    </div>
  );
};
