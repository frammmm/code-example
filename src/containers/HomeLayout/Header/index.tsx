import React from 'react';
import { Link, NavLink, RouteComponentProps } from 'react-router-dom';
import { withRouter } from 'react-router';
import { inject, observer } from 'mobx-react';
import cn from 'classnames/bind';
import qs from 'qs';

import {
  Button, Dropdown, DropdownMenu, Form, Icon,
} from 'components';
import { ThemeContext } from 'contexts';
import { history } from 'utils/history';
import styles from './index.module.scss';

const cx = cn.bind(styles);

const navMenuItems = [
  // { title: '...', to: '/' },
  // { title: '...', to: '/books' },
  // { title: '...', to: '/sites' },
];

interface IProps extends RouteComponentProps {
  authStore?: any;
}

interface IState {
  isContextMenuOpen: boolean;
  isSearchMenuOpen: boolean;
  isSearchRevealed: boolean;
  searchQuery: string;
}

@inject('authStore')
@observer
class HeaderComponent extends React.Component<IProps, IState> {
  searchInputRef = React.createRef<HTMLInputElement>();

  constructor(props: IProps) {
    super(props);

    const { location: { search } } = this.props;
    const { query = '' } = qs.parse(search.slice(1));
    const decodedQuery = decodeURIComponent(query);

    this.state = {
      isContextMenuOpen: false,
      isSearchMenuOpen: false,
      isSearchRevealed: false,
      searchQuery: decodedQuery,
    };
  }

  onChangeSearchText = (e: any) => {
    this.setState({
      searchQuery: e.target.value,
    });
  }

  clearSearchText = () => {
    this.setState({
      searchQuery: '',
      isSearchRevealed: false,
    });
  }

  onSearchBlur = () => {
    if (!this.state.searchQuery) {
      this.setState({ isSearchRevealed: false });
    }
  }

  onSearchFocus = () => {
    this.setState({ isSearchRevealed: true });
  }

  toggleContextMenu = () => {
    this.setState(({ isContextMenuOpen }) => ({
      isContextMenuOpen: !isContextMenuOpen,
      isSearchMenuOpen: false,
    }));
  }

  toggleSearchMenu = () => {
    this.setState(({ isSearchMenuOpen }) => ({
      isSearchMenuOpen: !isSearchMenuOpen,
      isContextMenuOpen: false,
    }), () => {
      if (this.state.isSearchMenuOpen && this.searchInputRef.current) {
        this.searchInputRef.current.focus();
      }
    });
  }

  navigateToSearchResults = ({ query }: any) => {
    if (!query) {
      return;
    }

    history.push(`/search?query=${encodeURIComponent(query)}`);
  }

  handleLogout = () => {
    this.props.authStore.logout();
  }

  renderContextMenu = () => {
    const D = Dropdown;

    return (
      <D.List>
        <D.ListItem>
          <D.Link to="/collection" onClick={this.toggleContextMenu}>...</D.Link>
        </D.ListItem>
        <D.ListItem>
          <D.Link to="/viewed" onClick={this.toggleContextMenu}>...</D.Link>
        </D.ListItem>
        <D.Separator />
        <D.ListItem>
          <D.Link as="span" onClick={this.handleLogout}>Выход</D.Link>
        </D.ListItem>
      </D.List>
    );
  }

  render() {
    const {
      isContextMenuOpen, isSearchMenuOpen, searchQuery, isSearchRevealed,
    } = this.state;
    return (
      <header className={cx('header')}>
        <span className={cx('logo-container')}>
          <Link to="/" className={cx('logo')} />
        </span>
        <Form onSubmit={this.navigateToSearchResults} className={cx('search-form')}>
          <div
            onClick={this.toggleSearchMenu}
            className={cx('mobile-search-button')}
          />
          <div className={cx('mobile-search-container', { open: isSearchMenuOpen })}>
            <span className={cx('search-input-container', 'is-grow-mobile', { revealed: isSearchRevealed })}>
              <input
                type="text"
                name="query"
                id="search-form-search"
                value={searchQuery}
                onChange={this.onChangeSearchText}
                onBlur={this.onSearchBlur}
                onFocus={this.onSearchFocus}
                placeholder="Поиск материалов"
                className={cx('search-input', { revealed: isSearchRevealed })}
                ref={this.searchInputRef}
              />
              <i
                onClick={this.clearSearchText}
                className={cx('icon')}
              />
            </span>
            <Button
              onClick={this.toggleSearchMenu}
              className={cx('search-cancel')}
              asLink
            >
              Отмена
            </Button>
          </div>
        </Form>
        <nav className={cx('nav')}>
          <ThemeContext.Consumer>
            {([{ primaryColor }]) => (
              <ul className={cx('nav-menu')}>
                {navMenuItems.map(({ title, to }) => (
                  <li
                    className={cx('nav-menu-item')}
                    key={title}
                  >
                    <NavLink to={to} exact activeClassName={cx('link-active')} style={{ borderColor: primaryColor }}>
                      {title}
                    </NavLink>
                  </li>
                ))}
              </ul>
            )
            }
          </ ThemeContext.Consumer>

        </nav>
        <div className={cx('menu')}>
          <div className={cx('visible-xs')}>
            <Icon
              className={cx('sidebar-button', { open: isContextMenuOpen })}
              name="gamburger"
              onClick={this.toggleContextMenu}
            />
            <div className={cx('sidebar-container', { open: isContextMenuOpen })}>
              {this.renderContextMenu()}
            </div>
          </div>
          <div className={cx('dropdown-menu-container')}>
            <DropdownMenu
              trigger={['click']}
              openClassName={cx('dropdown-opened')}
              overlayClassName={cx('dropdown-position')}
              align={{ offset: [-16, 22] }}
              closeOnSelect={false}
              overlay={this.renderContextMenu()}
              animation="slide-up"
            >
              <div className={cx('dropdown-closed')}>
                <div className={cx('dropdown-toggler')} data-test="profile-dropdown-toggler">Профиль</div>
              </div>
            </DropdownMenu>
          </div>
        </div>
      </header>
    );
  }
}

export const Header = withRouter<IProps, any>(HeaderComponent);
