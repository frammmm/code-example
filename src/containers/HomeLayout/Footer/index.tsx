import React from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames/bind';

import styles from './index.module.scss';

const cx = cn.bind(styles);

export const Footer = ({ className }: any) => (
  <footer className={cx('footer', className)}>
    <div className={cx('text-container')}>
      <div className={cx('left-inner')}>
        <div className={cx('text-support')}>
          <span>Поддержка: ...</span>
          <a href="...">...</a>
        </div>
        <div className={cx('text-agreement')}>
          <div>{'...'}</div>
          <Link to="/agreement">
              Условия использования.
          </Link>
        </div>
      </div>
      <div className={cx('right-inner')}>
        <a href="...">
          <span className={cx('app-store-icon')} />
        </a>
        <a href="...">
          <span className={cx('google-play-icon')} />
        </a>
      </div>
    </div>
    <div id="portal-padding" />
  </footer>
);
