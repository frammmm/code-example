import React from 'react';
import Media from 'react-responsive';
import { StickyContainer, Sticky } from 'react-sticky';
import { Link } from 'react-router-dom';
import cn from 'classnames/bind';
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
import { Breadcrumbs, ContentWrapper, Title } from 'components';
import { Breakpoints } from 'utils/constants';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  children?: React.ReactNode;
  name: string;
  url: string;
  number: string;
  thumbUrl: string;
  date: string;
}

export const IssueListWrapper = ({
  children,
  name: editionName,
  url: editionUrl,
  number,
  thumbUrl,
  date,
}: IProps) => {
  const formatedDate = format(new Date(date), 'd MMMM', { locale: ru });
  const firstPageUrl = '?view=slide&index=0';
  return (
    <div className={cx('container')}>
      <div className={cx('content')}>
        <Breadcrumbs>
          <Link to="/">...</Link>
          <Link to={editionUrl}>{editionName}</Link>
          <span>... &#8470;{number}</span>
        </Breadcrumbs>
        <Media maxDeviceWidth={Breakpoints.sm}>
          {(matches) => (
            <ContentWrapper className={cx('content-inner')}>
              <div className={cx('left-container')}>
                {matches && <Title className={cx('title')}as="h2">... &#8470;{number} от {formatedDate}</Title>}
                {matches ? (
                  <Link to={firstPageUrl}>
                    <img src={thumbUrl} className={cx('cover')} alt="first page cover" />
                  </Link>
                ) : (
                  <StickyContainer>
                    <Sticky topOffset={-90}>
                      {({ style, isSticky }) => (
                        <Link to={firstPageUrl} style={{ ...style, top: isSticky ? 90 : 0 }}>
                          <img src={thumbUrl} className={cx('cover')} alt="first page cover" />
                        </Link>
                      )}
                    </Sticky>
                  </StickyContainer>
                )}
              </div>
              <div className={cx('right-container')}>
                {!matches && <Title className={cx('title')}as="h2">... &#8470;{number} от {formatedDate}</Title>}
                {children}
              </div>
            </ContentWrapper>
          )}
        </Media>
      </div>
    </div>
  );
};
