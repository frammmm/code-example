import React from 'react';
import { inject, observer } from 'mobx-react';
import {
  Redirect,
  Route,
  RouteProps,
} from 'react-router-dom';

const RouteWithLayout = ({
  component = null,
  layoutProps,
  layout: Layout,
  routeProps,
}: any) => (
  <Layout layoutProps={layoutProps} routeProps={routeProps}>
    {component && React.createElement(component, routeProps)}
  </Layout>
);

interface IProps extends RouteProps {
  layout: React.ElementType<any>;
  layoutProps?: { [key: string]: any };
  access: 'auth' | 'private' | 'common';
}

export class AppRoute extends React.Component<IProps> {
  static defaultProps = {
    access: 'auth',
  }

  renderPage = inject('authStore')(
    observer(({ authStore, ...routeProps }: any) => {
      const { access, ...topLevelPropsRest } = this.props as any;
      if (access === 'private' && !authStore.isLoggedIn) {
        return <Redirect to="/login" />;
      }
      if (access === 'auth' && authStore.isLoggedIn) {
        return <Redirect to="/" />;
      }
      return <RouteWithLayout routeProps={routeProps } { ...topLevelPropsRest }/>;
    }),
  )

  render() {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { access, component, ...rest } = this.props;
    return (
      <Route
        component={this.renderPage}
        {...rest}
      />
    );
  }
}
