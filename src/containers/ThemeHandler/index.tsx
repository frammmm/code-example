import React from 'react';
import { Helmet } from 'react-helmet';
import { useTheme } from 'contexts';

export const ThemeHandler = () => {
  const [{ primaryColor }] = useTheme();
  if (primaryColor) {
    return (
      <Helmet>
        <style>
          {`
            a {
              color: ${primaryColor};
            }

            a:hover {
              color: ${primaryColor};
            }

            :checked {
              background-color: ${primaryColor} !important;
            }
          `}
        </style>
      </Helmet>
    );
  }
  return null;
};
