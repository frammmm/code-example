import React from 'react';
import cn from 'classnames/bind';
import { Button, Icon, Modal } from 'components';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  children: React.ReactNode;
}

interface IState {
  isModalOpen: boolean;
  imageSrc?: string;
}

export class ImageDialog extends React.Component<IProps, IState> {
  state = {
    isModalOpen: false,
    imageSrc: undefined,
  }

  handleDocumentClick = (event: React.MouseEvent) => {
    const { target } = event;

    if ((target as Element).nodeName.toLowerCase() === 'img') {
      event.preventDefault();

      this.openModalDialog((target as HTMLImageElement).src);
    }
  }

  openModalDialog = (imageSrc: string) => {
    this.setState({
      isModalOpen: true,
      imageSrc,
    });
  }

  closeModalDialog = () => {
    this.setState({
      isModalOpen: false,
      imageSrc: undefined,
    });
  }

  handleRequestClose = (e: any) => {
    if (e.keyCode === 27) {
      this.closeModalDialog();
    }
  }

  render() {
    const { children } = this.props;
    const { imageSrc, isModalOpen } = this.state;

    return (
      <>
        <Modal
          className={cx('modal')}
          bodyOpenClassName="is-overflow-hidden"
          overlayClassName={cx('overlay')}
          isOpen={isModalOpen}
          onRequestClose={this.handleRequestClose}
        >
          <div className={cx('body')}>
            <div>
              <img src={imageSrc} alt="" />
            </div>
          </div>
          <Button
            className={cx('close-btn')}
            color="transparent"
            onClick={this.closeModalDialog}
          >
            <Icon name="times-black-sm"/>
          </Button>
        </Modal>
        <div onClick={this.handleDocumentClick}>
          {children}
        </div>
      </>
    );
  }
}
