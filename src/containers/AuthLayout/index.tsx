import React from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames/bind';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  children?: React.ReactNode;
}

export const AuthLayout = ({ children }: IProps) => (
  <div className={cx('container')}>
    <header className={cx('header')}>
      <Link to="/" className={cx('logo')} />
    </header>
    <div className={cx('content')}>{children}</div>
    <footer className={cx('footer')}>
      <a href="..." target="_blank" rel="noopener noreferrer">
        {`...`}
      </a>
    </footer>
  </div>
);
