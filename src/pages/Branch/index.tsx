import React from 'react';
import { inject, observer } from 'mobx-react';
import { Link, RouteComponentProps } from 'react-router-dom';
import * as _ from 'lodash';
import cn from 'classnames/bind';
import { Api } from 'types';
import {
  Breadcrumbs, CardsList, ContentWrapper, EditionCard, Section, SectionLoader,
} from 'components';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface RouteParams {
  branchId: string;
}

interface IProps extends RouteComponentProps<RouteParams> {
  children?: React.ReactNode;
  periodicalsStore?: any;
}

@inject('periodicalsStore')
@observer
export class Branch extends React.Component<IProps> {
  public componentDidMount() {
    const { branchId } = this.props.match.params;
    this.props.periodicalsStore.fetchBranch(branchId);
  }

  public render() {
    const { branchStruct: { data, isFetching } } = this.props.periodicalsStore;
    const editions = _.get(data, ['data', 'editions']);
    const title = _.get(data, ['data', 'name']);
    if (!data || isFetching) {
      return (
        <div className={cx('container')}>
          <div className={cx('content-container')}>
            <SectionLoader breadcrumbs />
          </div>
        </div>
      );
    }
    return (
      <div className={cx('container')}>
        <div className={cx('content-container')}>
          <Breadcrumbs>
            <Link to="/">Периодика</Link>
            <Link to="/branches">Рубрики</Link>
            <span>{title}</span>
          </Breadcrumbs>
          <Section
            title={title}
            separator
          >
            <ContentWrapper>
              <CardsList.Container>
                {editions
                  // FIXME: editions should be on backend
                  .filter(({ latestIssue }: Api.Edition) => (latestIssue !== null))
                  .map((item: Api.Edition) => (
                    <CardsList.Item key={item.id}>
                      <EditionCard item={item} responsiveMobile />
                    </CardsList.Item>
                  ))}
              </CardsList.Container>
            </ContentWrapper>
          </Section>
        </div>
      </div>
    );
  }
}
