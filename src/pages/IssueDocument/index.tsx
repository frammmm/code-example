import React from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import * as _ from 'lodash';
import cn from 'classnames/bind';
import { StickyContainer, Sticky } from 'react-sticky';
import {
  Breadcrumbs,
  ContentWrapper,
  Dropdown as D,
  DropdownMenu,
  IssueTextLoader,
  Spinner,
  Title,
  toast,
} from 'components';
import { ImageDialog } from 'containers';
import { Api } from 'types';
import { mixpanel } from 'utils/mixpanel';
import { trackStatEvent } from 'utils/analytics';
import styles from './index.module.scss';

// eslint-disable-next-line global-require
const copy = require('copy-to-clipboard') as any;

const cx = cn.bind(styles);

interface IProps {
  children?: Element;
  periodicalsStore?: any;
  issueId: string;
  editionId: string;
  documentId: string;
}

interface IState {
  isShareLinkCopied: boolean;
  rightCoord?: number;
}

@inject('periodicalsStore')
@observer
export class IssueDocument extends React.Component<IProps, IState> {
  state = {
    isShareLinkCopied: false,
    rightCoord: undefined,
  }

  rightRef = React.createRef() as any

  shouldTrackDocumentOpen = true
  shouldTrackLinkShare = true

  componentDidMount() {
    const { documentId, periodicalsStore: { fetchDocument } } = this.props;
    fetchDocument(documentId);

    window.addEventListener('resize', this.onResize);
  }

  componentDidUpdate(prevProps: IProps) {
    const { documentId, periodicalsStore: { fetchDocument } } = this.props;
    if (documentId !== prevProps.documentId) {
      fetchDocument(documentId);
      this.shouldTrackDocumentOpen = true;
      this.shouldTrackLinkShare = true;
    }

    const {
      documentStruct: { data: documentData },
      issueStruct: { data: issueData },
    } = this.props.periodicalsStore;

    if (documentData && issueData) { this.trackDocumentOpen(); }

    this.updateCoords();
  }

  componentWillUnmount(): void {
    window.removeEventListener('resize', this.onResize);
  }

  trackDocumentOpen = () => {
    if (!this.shouldTrackDocumentOpen) { return; }

    const {
      editionId,
      issueId,
      documentId,
      periodicalsStore: {
        documentStruct: { data: documentData },
        issueStruct: { data: issueData },
      },
    } = this.props;

    trackStatEvent('document', {
      issueId: Number(issueId),
      documentId: Number(documentId),
      pages: documentData.pages.slice(0, 2),
    });
    mixpanel.track('document', {
      editionId: Number(editionId),
      issueId: Number(issueId),
      documentId: Number(documentId),
      documentName: documentData.title,
      pages: documentData.pages,
      issueName: `${issueData.edition.name}(${issueData.number})`,
    });
    this.shouldTrackDocumentOpen = false;
  };

  trackLinkShare = () => {
    if (!this.shouldTrackLinkShare) { return; }

    const {
      editionId,
      issueId,
      documentId,
      periodicalsStore: {
        documentStruct: { data: documentData },
        issueStruct: { data: issueData },
      },
    } = this.props;

    mixpanel.track('share_link_create', {
      editionId: Number(editionId),
      issueId: Number(issueId),
      documentId: Number(documentId),
      documentName: documentData.title,
      pages: documentData.pages,
      issueName: `${issueData.edition.name}(${issueData.number})`,
    });
    this.shouldTrackLinkShare = false;
  };

  private onResize = _.throttle(() => this.updateCoords(), 100);

  private updateCoords = () => {
    const { current } = this.rightRef;

    if (current) {
      const coords = current.getBoundingClientRect();

      if (coords.right !== this.state.rightCoord) {
        this.setState({
          rightCoord: coords.right,
        });
      }
    }
  }

  private handleDropdownVisibleChange = (isVisible: boolean) => {
    const { documentId, periodicalsStore } = this.props;
    const {
      shareLinkStruct: { data, isFetching },
      fetchShareLink,
    } = periodicalsStore;
    if (isVisible && !isFetching && !data) {
      fetchShareLink(documentId);
    }
  }

  private copyToClipboard = () => {
    const {
      shareLinkStruct: { data },
    } = this.props.periodicalsStore;
    const link = _.get(data, 'link');
    if (link) {
      copy(link);
      toast.info('Ссылка скопирована!');
      this.setState({ isShareLinkCopied: true });
    }
  };

  private findNeighbooringDocumentIds(id: string, documents: Api.IssueDetails.Document[]) {
    const docIdx = _.findIndex(documents, { id: Number(id) });
    return [
      _.get(documents, [docIdx - 1, 'id']),
      _.get(documents, [docIdx + 1, 'id']),
    ];
  }

  private renderContextMenu = () => {
    const {
      shareLinkStruct: { data },
    } = this.props.periodicalsStore;
    const shareLink = _.get(data, 'link');
    if (!shareLink) {
      return (
        <D.List><Spinner color="primary" /></D.List>
      );
    }
    const { isShareLinkCopied } = this.state;
    const sendToEmailUrl = `mailto:?body=${encodeURIComponent(shareLink)}`;
    return (
      <D.List>
        <D.ListItem>
          <D.Link
            as="span"
            onClick={this.copyToClipboard}
            className={cx('share-link-item')}
            id="test-copy-share-link"
          >
            {isShareLinkCopied ? 'Скопировано' : 'Скопировать'}
          </D.Link>
        </D.ListItem>
        <D.ListItem>
          <D.Link
            as="a"
            href={sendToEmailUrl}
            className={cx('share-link-item')}
            id="test-share-doc-via-email"
          >
            Отправить на e-mail
          </D.Link>
        </D.ListItem>
      </D.List>
    );
  }

  render() {
    const {
      documentStruct: { data: documentData, isFetching: isDocumentFetching },
      issueStruct: { data: issueData, isFetching: isIssueFetching },
    } = this.props.periodicalsStore;

    if (!documentData || isDocumentFetching || !issueData || isIssueFetching) {
      return (
        <div className={cx('container')}>
          <div className={cx('content-container')}>
            <IssueTextLoader />
          </div>
        </div>
      );
    }

    const { body, title, authors } = documentData;
    const { edition: { url: editionUrl, name: editionName }, number: issueNumber } = issueData;
    const { rightCoord } = this.state;
    const { documents } = issueData;
    const [
      prevDocumentId, nextDocumentId,
    ] = this.findNeighbooringDocumentIds(this.props.documentId, documents);
    const linkToPrevDoc = (
      <Link
        to={`?view=doc&id=${prevDocumentId}`}
        className={cx('arrow-link')}
        data-test="test-prev-doc-link"
      >
        <span className={cx('arrow-left')}>&lt;-</span>
        <span>Предыдущая<br />статья</span>
      </Link>
    );
    const linkToNextDoc = (
      <Link
        to={`?view=doc&id=${nextDocumentId}`}
        className={cx('arrow-link')}
        data-test="test-next-doc-link"
      >
        <span>Следующая<br />статья</span>
        <span className={cx('arrow-right')}>-&gt;</span>
      </Link>
    );
    return (
      <div className={cx('container')}>
        <div className={cx('content-container')}>
          <Breadcrumbs>
            <Link to="/">Периодика</Link>
            <Link to={editionUrl}>{editionName}</Link>
            <span>Выпуск &#8470;{issueNumber}</span>
            <span>{title}</span>
          </Breadcrumbs>
          <ContentWrapper>
            <div ref={this.rightRef} className={cx('content-inner')}>
              <div className={cx('left-container')}>
                {prevDocumentId && (
                  <StickyContainer >
                    <Sticky>
                      {({ style }) => (
                        <div style={{ ...style, top: '50%', position: 'fixed' }}>
                          {linkToPrevDoc}
                        </div>
                      )}
                    </Sticky>
                  </StickyContainer>
                )}
              </div>
              <div className={cx('center-container')}>
                <Title as="h2">{title}</Title>
                <div className={cx('subtitle-container')}>
                  {authors && <span>Автор: {authors}</span>}
                  <DropdownMenu
                    trigger={['click']}
                    openClassName={cx('dropdown-opened')}
                    overlayClassName={cx('dropdown-position')}
                    align={{ offset: [-16, 22] }}
                    closeOnSelect={false}
                    overlay={this.renderContextMenu()}
                    animation="slide-up"
                    onVisibleChange={this.handleDropdownVisibleChange}
                  >
                    <D.Toggler id="test-share-dropdown">Поделиться</D.Toggler>
                  </DropdownMenu>
                </div>
                <ImageDialog>
                  <div dangerouslySetInnerHTML={{ __html: body }} />
                </ImageDialog>
                <div className={cx('mobile-controls')}>
                  {linkToPrevDoc}
                  {linkToNextDoc}
                </div>
              </div>
              <div className={cx('right-container')}>
                {nextDocumentId && (
                  <StickyContainer>
                    {rightCoord && (
                      <Sticky>
                        {({ style }) => (
                          <div
                            style={{
                              ...style,
                              position: 'fixed',
                              top: '50%',
                              right: `calc(100% - ${rightCoord}px)`,
                              left: 'auto',
                              width: 'auto',
                            }}
                          >
                            {linkToNextDoc}
                          </div>
                        )}
                      </Sticky>
                    )}
                  </StickyContainer>
                )}
              </div>
            </div>
          </ContentWrapper>
        </div>
      </div>
    );
  }
}
