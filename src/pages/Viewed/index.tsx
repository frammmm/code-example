import React from 'react';
import { inject, observer } from 'mobx-react';
import { InView } from 'react-intersection-observer';
import cn from 'classnames/bind';
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
import {
  CardsList, ContentWrapper, IssueCard, Section, SectionLoader,
} from 'components';
import { Api, Stores } from 'types';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  children?: React.ReactNode;
  periodicalsStore: Stores.IPeriodicalsStore;
}

@inject('periodicalsStore')
@observer
export class Viewed extends React.Component<IProps> {
  public componentDidMount() {
    this.props.periodicalsStore.fetchViewedIfNeeded();
  }

  handleListBottomIntersection = (inView: boolean) => {
    if (inView) {
      this.props.periodicalsStore.fetchViewedIfNeeded();
    }
  }

  renderCards(cards: Api.Issue[]) {
    return (
      <div>
        <CardsList.Container className={cx('cards-list')}>
          {cards.map((item: Api.Issue) => (
            <CardsList.Item className={cx('card')} key={item.id}>
              <IssueCard item={item} renderDate responsiveMobile/>
            </CardsList.Item>
          ))}
        </CardsList.Container>
      </div>
    );
  }

  getSectionTitle(item: Api.EditionPeriod) {
    switch (item.state) {
      case 'current': {
        return 'Сегодня';
      }
      case 'last': {
        return 'На прошлой неделе';
      }
      case 'old': {
        const startDate = format(new Date(item.startDate), 'd MMMM', { locale: ru });
        const endDate = format(new Date(item.endDate), 'd MMMM', { locale: ru });

        return `${startDate} - ${endDate}`;
      }
      default: {
        return '';
      }
    }
  }

  render() {
    const {
      viewed: { data },
      viewedStruct: { isFetching },
    } = this.props.periodicalsStore;

    if (!data || isFetching) {
      return (
        <div className='container'>
          <div className='content-container'>
            <SectionLoader />
          </div>
        </div>
      );
    }

    return (
      <>
        {data && data.map((item: Api.EditionPeriod, index) => (
          <Section
            key={index}
            separator={index !== data.length - 1}
            title={this.getSectionTitle(item)}
            titleAs="h2"
            titleClassName={cx('title', 'is-marginless')}
            topPadding={index === 0 && 'medium'}
          >
            <ContentWrapper>
              {item.issues && this.renderCards(item.issues)}
            </ContentWrapper>
          </Section>
        ))}
        <InView onChange={this.handleListBottomIntersection} rootMargin="100px">
          <div />
        </InView>
      </>
    );
  }
}
