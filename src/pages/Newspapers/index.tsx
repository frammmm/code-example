import React from 'react';
import { inject, observer } from 'mobx-react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { InView } from 'react-intersection-observer';
import cn from 'classnames/bind';
import { Api, Stores } from 'types';
import {
  Breadcrumbs, CardsList, ContentWrapper, IssueCard, Section, SectionLoader,
} from 'components';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface RouteParams {
  branchId: string;
}

interface IProps extends RouteComponentProps<RouteParams> {
  children?: React.ReactNode;
  periodicalsStore: Stores.IPeriodicalsStore;
}

@inject('periodicalsStore')
@observer
export class Newspapers extends React.Component<IProps> {
  public componentDidMount() {
    this.props.periodicalsStore.fetchNewspapersIfNeeded();
  }

  handleListBottomIntersection = (inView: boolean) => {
    if (inView) {
      this.props.periodicalsStore.fetchNewspapersIfNeeded();
    }
  }

  public render() {
    const {
      newspapers: { data: issues },
      newspapersStruct: { isFetching },
    } = this.props.periodicalsStore;

    if (!issues.length && isFetching) {
      return (
        <div className={cx('container')}>
          <div className={cx('content-container')}>
            <SectionLoader breadcrumbs />
          </div>
        </div>
      );
    }

    return (
      <div className={cx('container')}>
        <div className={cx('content-container')}>
          <Breadcrumbs>
            <Link to="/">Периодика</Link>
            <span>Свежие выпуски газет</span>
          </Breadcrumbs>
          <Section
            title="Свежие выпуски газет"
            separator
          >
            <ContentWrapper>
              <CardsList.Container>
                {issues && issues
                  .map((item: Api.Issue) => (
                    <CardsList.Item key={item.id}>
                      <IssueCard item={item} responsiveMobile />
                    </CardsList.Item>
                  ))}
              </CardsList.Container>
            </ContentWrapper>
            <InView onChange={this.handleListBottomIntersection} rootMargin="100px">
              <div className={cx('list-bottom')} />
            </InView>
          </Section>
        </div>
      </div>
    );
  }
}
