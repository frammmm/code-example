import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import cn from 'classnames/bind';
import { Title } from 'components';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  children?: React.ReactNode;
  authStore?: any;
}

interface IState {
  isAgreementConfirmed: boolean;
}

export class LibsList extends React.Component<IProps, IState> {
  public render() {
    const libs = [
      { title: '...' },
    ];
    if (libs.length === 1) {
      return <Redirect to="/" />;
    }
    return (
      <div className={cx('container')}>
        <div className={cx('content-container')}>
          <Title as="h2" center>
            ...
          </Title>
          <div>
            <ul className={cx('list')}>
              {libs.map(({ title }) => (
                <li key={title}>
                  <Link to="#"> {title} </Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
