import React from 'react';
import * as _ from 'lodash';
import Media from 'react-responsive';
import { inject, observer } from 'mobx-react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Redirect } from 'react-router';
import cn from 'classnames/bind';

import {
  Button, BottomFixedBar, Icon, Tooltip,
} from 'components';
import { getQueryParams } from 'utils/urls';
import { Breakpoints } from 'utils/constants';
import { mixpanel } from 'utils/mixpanel';
import { trackStatEvent } from 'utils/analytics';
import { HomeLayout } from 'containers';
import { Stores } from 'types';
import { IssuePreview } from '../IssuePreview';
import { IssueTextList } from '../IssueTextList';
import { IssueDocument } from '../IssueDocument';
import { IssueSlide } from '../IssueSlide';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface RouteParams {
  issueId: string;
  editionId: string;
}

interface IProps {
  periodicalsStore: Stores.IPeriodicalsStore;
  routeProps: RouteComponentProps<RouteParams>;
}

interface IState {
  showTips: boolean | undefined;
}

@inject('periodicalsStore')
@observer
export class IssueRouter extends React.Component<IProps, IState> {
  state = {
    showTips: undefined,
  }

  componentDidMount() {
    const { issueId } = this.props.routeProps.match.params;
    const { issueStruct: { data, isFetching } } = this.props.periodicalsStore;

    const isIssueChanged = Number(issueId) !== _.get(data, 'id');
    if ((!data || isIssueChanged) && !isFetching) {
      this.props.periodicalsStore.fetchIssue(issueId);
    }
  }

  componentDidUpdate() {
    if (!this.props.periodicalsStore.issueStruct.isFetching && this.state.showTips === undefined) {
      const isTipShown = localStorage.getItem('isIssueTipClosed');

      if (!isTipShown) {
        setTimeout(() => {
          this.setState({
            showTips: true,
          });
        }, 1500);
      }
    }
  }

  componentWillUnmount() {
    const issueId = Number(this.props.routeProps.match.params.issueId);
    trackStatEvent('exit', { issueId });
    mixpanel.track('exit', { issueId });
  }

  private handleTipShow = () => {
    document.body.classList.add('is-faded');
    document.body.classList.add('is-faded-gray');
  }

  private handleTipClose = () => {
    localStorage.setItem('isIssueTipClosed', 'true');

    this.setState({
      showTips: false,
    });

    document.body.classList.remove('is-faded');
    document.body.classList.remove('is-faded-gray');
  }

  private getNextViewMode = () => {
    const { view, id } = getQueryParams(this.props.routeProps.location.search);
    const isTextMode = view === 'text' || view === undefined;
    if (view === 'doc') {
      return 'slide';
    }
    if (id) {
      return isTextMode ? 'slide' : 'doc';
    }
    return isTextMode ? 'preview' : 'text';
  }

  private getNextViewModeUrl = () => {
    const { documentStruct: { data } } = this.props.periodicalsStore;
    const { view } = getQueryParams(this.props.routeProps.location.search);
    const nextMode = this.getNextViewMode();
    const documentPage = data && Math.trunc(data.pages.slice().sort()[0] / 2.0);
    if (view === 'doc' && nextMode === 'slide') {
      return `?view=${nextMode}&index=${documentPage}`;
    }
    return `?view=${nextMode}`;
  }

  private renderTip = () => (
    <div className={cx('tip-content')}>
      <p>...</p>
      <Button onClick={this.handleTipClose} round="md">Ок</Button>
    </div>
  )

  private renderBottomBar = () => {
    const { showTips } = this.state;

    const nextViewMode = this.getNextViewMode();
    const nextViewModeUrl = this.getNextViewModeUrl();
    const isNextModeText = nextViewMode === 'text' || nextViewMode === 'doc';
    const toggleViewMessage = isNextModeText ? '...' : '...';
    const toggleViewMobileMessage = isNextModeText ? '...' : '...';

    return (
      <BottomFixedBar className={cx({ 'show-tips': !!showTips })}>
        <div className={cx('bottom-bar-left')} />
        <div className={cx('bottom-bar-right')}>
          <Media maxDeviceWidth={Breakpoints.sm}>
            {(matches: boolean) => (
              <Tooltip
                className={cx('bottom-bar-tooltip')}
                content={this.renderTip()}
                onHidden={this.handleTipClose}
                onShow={this.handleTipShow}
                visible={!!showTips}
              >
                <span>
                  <Link
                    onClick={this.handleTipClose}
                    className={cx('bottom-bar-link')}
                    to={nextViewModeUrl}
                    id="test-toggle-view-mode"
                  >
                    <Icon name={nextViewMode === 'text' ? 'lines' : 'newspaper'} className={cx('bottom-bar-icon')} />
                    <span className={cx('bottom-bar-text')}>{matches ? toggleViewMobileMessage : toggleViewMessage}</span>
                  </Link>
                </span>
              </Tooltip>
            )}
          </Media>
        </div>
      </BottomFixedBar>
    );
  }

  render() {
    // subscribe to observable object to run didUpdate when it needs
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { isFetching } = this.props.periodicalsStore.issueStruct;
    const { issueId, editionId } = this.props.routeProps.match.params;
    const {
      view, id, index, page,
    } = getQueryParams(this.props.routeProps.location.search);
    const issueUrl = `/editions/${editionId}/issues/${issueId}/`;

    if (view === 'doc') {
      if (id === undefined) { return <Redirect to={issueUrl} />; }
      return (
        <HomeLayout layoutProps={{ bottomOffsetForIssue: true }}>
          <IssueDocument issueId={issueId} editionId={editionId} documentId={id} />
          {this.renderBottomBar()}
        </HomeLayout>
      );
    }

    if (view === 'slide') {
      if (index === undefined) { return <Redirect to={issueUrl} />; }
      return (
        <IssueSlide issueId={issueId} editionId={editionId} spreadIdx={index} page={page} />
      );
    }

    if (view === 'preview') {
      return (
        <HomeLayout layoutProps={{ bottomOffsetForIssue: true }}>
          <IssuePreview issueId={issueId} editionId={editionId} />
          {this.renderBottomBar()}
        </HomeLayout>
      );
    }

    return (
      <HomeLayout layoutProps={{ bottomOffsetForIssue: true }}>
        <IssueTextList issueId={issueId} editionId={editionId} />
        {this.renderBottomBar()}
      </HomeLayout>
    );
  }
}
