/* eslint-disable max-len */
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import cn from 'classnames/bind';
import { Button, Title } from 'components';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  routeProps: RouteComponentProps;
}


export const Agreement = ({ routeProps: { history } }: IProps) => (
  <div className={(cx('container'))}>
    <div className={(cx('content-container'))}>
      <Title as="h2">...</Title>
      <ol className={cx('list')}>
        <li>
          ...
        </li>
      </ol>
      <i>...</i>
      <Button
        className={cx('button')}
        onClick={() => { history.push('/'); }}
      >
         Назад
      </Button>
    </div>
  </div>
);
