import React from 'react';
import Media from 'react-responsive';
import { Link, RouteComponentProps } from 'react-router-dom';
import { StickyContainer, Sticky } from 'react-sticky';
import { inject, observer } from 'mobx-react';
import * as _ from 'lodash';
import cn from 'classnames/bind';
import { Emoji } from 'emoji-mart';
import { Api, Stores } from 'types';
import {
  Button,
  ButtonLink,
  Breadcrumbs,
  ContentWrapper,
  LinesEllipsis,
  Title,
  BookLoader,
} from 'components';

import styles from './index.module.scss';
import { Breakpoints } from '../../utils/constants';

const ReactJWPlayer = require('react-jw-player').default;

const cx = cn.bind(styles);

interface RouteParams {
  bookId: string;
}

interface IProps extends RouteComponentProps<RouteParams> {
  booksStore: Stores.IBooksStore;
}

interface IState {
  vote: Api.Book.Vote;
}

const Section = ({ title, children, separator = false }: any) => (
  <div className={cx('section', { separator })}>
    <span className={cx('section-title')}>{title}</span>
    {children}
  </div>
);

@inject('booksStore')
@observer
export class Book extends React.Component<IProps, IState> {
  state = {
    vote: null,
  }

  static getDerivedStateFromProps: React.GetDerivedStateFromProps<IProps, IState> =
  (props: IProps, prevState: IState) => {
    const { bookStruct: { data } } = props.booksStore;
    const vote = _.get(data, 'vote', null);

    if (prevState.vote === null && vote !== null) {
      return { vote };
    }

    return prevState;
  }

  componentDidMount() {
    const { bookId } = this.props.match.params;
    this.props.booksStore.fetchBook(bookId);
  }

  componentWillUnmount() {
    this.props.booksStore.clearStructs();
  }

  rateBook = (newVote: Api.Book.Vote) => {
    const { vote } = this.state;
    const { bookId } = this.props.match.params;
    const { rateBook } = this.props.booksStore;
    rateBook({ bookId, vote: newVote, alreadyVoted: vote !== null });
  }

  renderVoteButtons() {
    const { vote } = this.state;
    const {
      bookStruct: { data },
      bookVoteStruct: { isFetching: isRateFetching },
    } = this.props.booksStore;

    if (!data) return null;

    return (
      <>
        <Button
          onClick={() => { this.rateBook('like'); }}
          disabled={vote === 'like'}
          loading={isRateFetching}
          color="white"
          className={cx('vote-button')}
          title="+1"
        >
          <Emoji emoji='thumbsup' set='apple' size={19} />
          <span className={cx('vote-number')}>{data.likes}</span>
        </Button>
        <Button
          onClick={() => { this.rateBook('dislike'); }}
          disabled={vote === 'dislike'}
          loading={isRateFetching}
          color="white"
          className={cx('vote-button')}
          title="-1"
        >
          <Emoji emoji='tomato' set='apple' size={19} />
          <span className={cx('vote-number')}>{data.dislikes}</span>
        </Button>
      </>
    );
  }

  renderBookInfo() {
    const {
      bookStruct: { data },
    } = this.props.booksStore;

    if (!data) return null;

    return (
      <>
        <Section title="О книге" separator>
          <LinesEllipsis
            text={data.description}
            maxLine={5}
            ellipsis='...'
            trimRight
            basedOn='words'
          />
        </Section>
        <Section title="Обзор" separator>
          <ReactJWPlayer
            playerId="book-review-video"
            playerScript="https://cdn.jwplayer.com/libraries/7d2a8S5x.js"
            file={data.reviewVideo}
          />
        </Section>
        <Section title="Жанры">
          {data.genres.map((name) => (
            <span className={cx('tag')} key={name}>{name}</span>
          ))}
        </Section>
        <Section title="Темы">
          {data.themes.map((name) => (
            <span className={cx('tag')} key={name}>{name}</span>
          ))}
        </Section>
      </>
    );
  }

  renderDesktop() {
    const {
      bookStruct: { data },
    } = this.props.booksStore;

    if (!data) return null;

    return (
      <div className={cx('container')}>
        <div className={cx('content')}>
          <Breadcrumbs>
            <Link to="/books">Книги</Link>
            <span>{data.title}</span>
          </Breadcrumbs>
          <ContentWrapper className={cx('content-inner')}>
            <div className={cx('left-container')}>
              <StickyContainer>
                <Sticky topOffset={-90}>
                  {({ style, isSticky }) => (
                    <Link
                      className={cx('cover-container')}
                      to={data.bookUrl}
                      style={{ ...style, top: isSticky ? 90 : 0 }}
                    >
                      <img src={data.cover} className={cx('cover')} alt="" />
                    </Link>
                  )}
                </Sticky>
              </StickyContainer>
            </div>
            <div className={cx('right-container')}>
              <Title as="h2" className={cx('title')}>{data.title}</Title>
              <div className={cx('buttons-container')}>
                <ButtonLink
                  className={cx('button-read')}
                  to={data.bookUrl}
                  id="test-read-book-link"
                >
                  Читать
                </ButtonLink>
                {this.renderVoteButtons()}
              </div>
              <div className={cx('info-container')}>
                <div>Автор: {data.author}</div>
                <div>Издательство: {data.publisher}</div>
              </div>
              {this.renderBookInfo()}
            </div>
          </ContentWrapper>
        </div>
      </div>
    );
  }

  renderMobile() {
    const {
      bookStruct: { data },
    } = this.props.booksStore;

    if (!data) return null;

    return (
      <div className={cx('container')}>
        <div className={cx('content')}>
          <ContentWrapper className={cx('top-container')}>
            <Link className={cx('back')} to='/books'>Назад</Link>
            <Link className={cx('cover-container')} to={data.bookUrl}>
              <img src={data.cover} className={cx('cover')} alt="" />
            </Link>
            <Title as="h2" className={cx('title')}>{data.title}</Title>
            <div className={cx('author')}>
              {data.author}
            </div>
            <div>
              {data.publisher}
            </div>
          </ContentWrapper>
          <ContentWrapper className={cx('bottom-container')}>
            <div className={cx('buttons-container')}>
              <div className={cx('useful')}>
                Полезна ли вам эта книга? Проголосуйте
              </div>
              {this.renderVoteButtons()}
            </div>
            {this.renderBookInfo()}
          </ContentWrapper>
        </div>
      </div>
    );
  }

  render() {
    const {
      bookStruct: { data, isFetching },
    } = this.props.booksStore;

    if (!data || isFetching) {
      return <BookLoader />;
    }

    return (
      <Media maxDeviceWidth={Breakpoints.sm}>
        {(matches) => (matches ? this.renderMobile() : this.renderDesktop())}
      </Media>
    );
  }
}
