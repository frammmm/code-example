import React from 'react';
import { inject, observer } from 'mobx-react';
import cn from 'classnames/bind';

import {
  CardsList,
  ContentWrapper,
  EditionCard,
  Section,
  SectionLoader,
} from 'components';
import { Api, Stores } from 'types';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  children?: React.ReactNode;
  periodicalsStore: Stores.IPeriodicalsStore;
}

@inject('periodicalsStore')
@observer
export class Collection extends React.Component<IProps> {
  componentDidMount() {
    this.props.periodicalsStore.fetchCollection();
  }

  public render() {
    const {
      collectionStruct: { data, isFetching },
    } = this.props.periodicalsStore;

    if (!data || isFetching) {
      return (
        <div className={cx('container')}>
          <div className={cx('content-container')}>
            <SectionLoader />
          </div>
        </div>
      );
    }

    const { data: editions } = data;

    return (
      <Section title="Моя коллекция" titleAs="h2" verticalPaddings="medium">
        <ContentWrapper>
          <CardsList.Container className={cx('cards-list')}>
            {editions.map((item: Api.Edition) => (
              <CardsList.Item className={cx('card')} key={item.id}>
                <EditionCard item={item} responsiveMobile shadows={false} />
              </CardsList.Item>
            ))}
          </CardsList.Container>
        </ContentWrapper>
      </Section>
    );
  }
}
