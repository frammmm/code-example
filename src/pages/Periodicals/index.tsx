import React from 'react';
import * as _ from 'lodash';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import cn from 'classnames/bind';
import Dialog from 'rc-dialog';
import 'rc-dialog/assets/index.css';
import {
  Button, CardsList, ContentWrapper, IssueCard, Section, SectionLoader, Title,
} from 'components';
import { Api } from 'types';
import './index.scss';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IState {
  isModalVisible: boolean;
}

interface IProps {
  children?: React.ReactNode;
  periodicalsStore?: any;
}

@inject('periodicalsStore')
@observer
export class Periodicals extends React.Component<IProps, IState> {
  state = {
    isModalVisible: false,
  }

  public componentDidMount() {
    this.props.periodicalsStore.fetchPeriodicals();
  }

  private openModal = () => {
    this.setState({ isModalVisible: true });
  }

  private closeModal = () => {
    this.setState({ isModalVisible: false });
  }

  private renderBranchesColumns(branchesColumns: Api.Branch[][]) {
    return (
      <div className={cx('branches-list')}>
        {branchesColumns.map((col: any, colIdx) => (
          <div key={colIdx} className={cx('branch-column')} id="test-branches-links-container">
            {col.map((item: any, rowIdx: number) => (
              rowIdx === 0 && colIdx === 0 ? (
                <Link to="/branches" key="all">Все издания</Link>
              ) : (
                <Link to={item.url} key={item.id}>{item.name}</Link>
              )))}
          </div>
        ))}
      </div>
    );
  }

  private renderBranchesList(branches: Api.Branch[]) {
    return (
      <div className={cx('branches-list')}>
        <Link to="/branches" key="all">Все издания</Link>
        {branches.map((item: Api.Branch) => (
          <Link to={item.url} key={item.id}>{item.name}</Link>
        ))}
      </div>
    );
  }

  public render() {
    const { periodicalsStruct: { data, isFetching } } = this.props.periodicalsStore;

    if (!data || isFetching) {
      return (
        <div className={cx('container')}>
          <div className={cx('content-container')}>
            <SectionLoader />
          </div>
        </div>
      );
    }

    const {
      collectionLatest, latestMagazines, latestNewspapers, branches,
    } = data || {} as any;
    const branchesColumns = _.chunk([{}, ...branches], Math.ceil((branches.length + 1) / 4));

    return (
      <>
        <div className={cx('content-container')}>
          {!_.isEmpty(collectionLatest) && (
            <Section
              gradient
              title={
                <>
                  <span className="is-hidden-xs">{'Свежие выпуски в моей коллекции'}</span>
                  <span className="is-visible-xs">{'Моя коллекция'}</span>
                </>
              }
              subtitle={
                <span className="is-hidden-xs">
                  {'Мы собрали для вас свежие выпуски ваших любимых изданий'}
                </span>
              }
              link={
                <Link to="/collection" id="test-user-collection-link">
                  <span className="is-hidden-xs">
                    {'Перейти в «Моя коллекция»'}
                  </span>
                  <span className="is-visible-xs">
                    {'Все'}
                  </span>
                </Link>
              }
            >
              <ContentWrapper>
                <CardsList.Container maxItemsPerRow={5} mobileGallery>
                  {collectionLatest.map((item: Api.Issue) => (
                    <CardsList.Item key={item.id}>
                      <IssueCard item={item} />
                    </CardsList.Item>
                  ))}
                </CardsList.Container>
              </ContentWrapper>
            </Section>
          )}
          <Section
            title="Рубрики"
            separator
          >
            <ContentWrapper>
              <div className={cx('branch-container')}>
                {this.renderBranchesColumns(branchesColumns)}
              </div>
              <Button
                onClick={this.openModal}
                fullwidth
                className={cx('reveal-branches')}
              >
                Показать рубрики
              </Button>
              <Dialog
                wrapClassName={cx('dialog')}
                visible={this.state.isModalVisible}
                onClose={this.closeModal}
              >
                <Title as="h4">Рубрики</Title>
                {this.renderBranchesList(branches)}
              </Dialog>
            </ContentWrapper>
          </Section>
          {!_.isEmpty(latestNewspapers) && (
            <Section
              title="Свежая газета"
              separator
              link={
                <Link to="/newspapers" id="test-all-newspapers-link">
                  <span className='is-hidden-xs'>
                    {'Все газеты'}
                  </span>
                  <span className='is-visible-xs'>
                    {'Все'}
                  </span>
                </Link>
              }
            >
              <ContentWrapper>
                <CardsList.Container maxItemsPerRow={5} mobileGallery>
                  {latestNewspapers.map((item: Api.Issue) => (
                    <CardsList.Item key={item.id}>
                      <IssueCard item={item} />
                    </CardsList.Item>
                  ))}
                </CardsList.Container>
              </ContentWrapper>
            </Section>
          )}
          {!_.isEmpty(latestMagazines) && (
            <Section
              title="Свежие журналы"
              link={
                <Link to="/magazines" id="test-all-magazines-link">
                  <span className='is-hidden-xs'>
                    {'Все журналы'}
                  </span>
                  <span className='is-visible-xs'>
                    {'Все'}
                  </span>
                </Link>
              }
            >
              <ContentWrapper>
                <CardsList.Container maxItemsPerRow={5} mobileGallery>
                  {latestMagazines.map((item: Api.Issue) => (
                    <CardsList.Item key={item.id}>
                      <IssueCard item={item} />
                    </CardsList.Item>
                  ))}
                </CardsList.Container>
              </ContentWrapper>
            </Section>
          )}
        </div>
      </>
    );
  }
}
