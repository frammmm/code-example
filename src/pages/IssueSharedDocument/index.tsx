import React from 'react';
import * as _ from 'lodash';
import { RouteComponentProps } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import cn from 'classnames/bind';
import { Stores } from 'types';
import {
  ContentWrapper,
  IssueTextLoader,
  Title,
} from 'components';
import { ImageDialog } from 'containers';
import { history } from 'utils/history';
import { makeIssueDocUrl } from 'utils/urls';
import styles from '../IssueDocument/index.module.scss';

const cx = cn.bind(styles);

const Container = ({ children }: any) => (
  <div className={cx('container')}>
    <div className={cx('content-container')}>
      <ContentWrapper>
        <div className={cx('content-inner')}>
          <div className={cx('left-container')} />
          <div className={cx('center-container')}>
            {children}
          </div>
          <div className={cx('right-container')} />
        </div>
      </ContentWrapper>
    </div>
  </div>
);

interface RouteParams {
  documentHash: string;
}

interface IProps extends RouteComponentProps<RouteParams> {
  periodicalsStore: Stores.IPeriodicalsStore;
  authStore: Stores.IAuthStore;
}

@inject('periodicalsStore', 'authStore')
@observer
export class IssueSharedDocument extends React.Component<IProps> {
  componentDidMount() {
    const {
      match,
      periodicalsStore: { fetchSharedDocument },
    } = this.props;
    fetchSharedDocument(match.params.documentHash);
  }

  componentDidUpdate() {
    const {
      periodicalsStore: { sharedDocumentStruct: { data } },
      authStore: { isLoggedIn },
    } = this.props;
    if (data) {
      const { id, editionId, issueId } = data;
      if (id && isLoggedIn) {
        history.replace(makeIssueDocUrl({ id, editionId, issueId }));
      }
    }
  }

  renderInvitation = () => (
    <div style={{ marginTop: '60px', borderTop: '1px solid #dedede', paddingTop: '30px' }}>
      <Title as="h4">
        ...
      </Title>
      ...
    </div>
  )

  render() {
    const {
      sharedDocumentStruct: { data, errors, isFetching },
    } = this.props.periodicalsStore;

    const errMsg = _.get(errors, ['data', 'message']);
    if (errMsg) {
      return (
        <Container>
          <Title as="h2">{errMsg}</Title>
          {this.renderInvitation()}
        </Container>
      );
    }

    if (!data || isFetching) {
      return (
        <div className={cx('container')}>
          <div className={cx('content-container')}>
            <IssueTextLoader />
          </div>
        </div>
      );
    }

    const { body, title, authors } = data;
    return (
      <div className={cx('container')}>
        <div className={cx('content-container')}>
          <ContentWrapper>
            <div className={cx('content-inner')}>
              <div className={cx('left-container')} />
              <div className={cx('center-container')}>
                <Title as="h2">{title}</Title>
                <div className={cx('subtitle-container')}>
                  {authors && <span>Автор: {authors}</span>}
                </div>
                <ImageDialog>
                  <div dangerouslySetInnerHTML={{ __html: body }} />
                </ImageDialog>
                {this.renderInvitation()}
              </div>
              <div className={cx('right-container')} />
            </div>
          </ContentWrapper>
        </div>
      </div>
    );
  }
}
