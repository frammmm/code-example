import React, { CSSProperties } from 'react';
import Media from 'react-responsive';
import { Sticky, StickyContainer } from 'react-sticky';
import { RouteComponentProps } from 'react-router';
import { InView } from 'react-intersection-observer';
import { toJS } from 'mobx';
import { inject, observer } from 'mobx-react';
import { Formik, FormikValues } from 'formik';
import cn from 'classnames/bind';
import _ from 'lodash';
import { disableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';

import {
  Button,
  BookReviewFormLoader,
  BookReviewListLoader, BookReviewLoader,
  CardsList,
  FormControl,
  Icon,
  ReviewCard,
  Title,
} from 'components';
import { Api, Stores } from 'types';
import { Breakpoints, KeyCodes } from 'utils/constants';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps extends RouteComponentProps {
  children?: React.ReactNode;
  booksStore: Stores.IBooksStore;
}

interface IState {
  isFilterOpen: boolean;
  hasValues: boolean;
  hasChangedValues: boolean;
  searchQuery?: string;
}

@inject('booksStore')
@observer
export class BookReviewsList extends React.Component<IProps, IState> {
  formValues: { [key: string]: any } = {};
  renderedOnce = false;
  sidebarRef = React.createRef<HTMLDivElement>();
  submitForm = () => {};

  state = {
    searchQuery: '',
    isFilterOpen: false,
    hasChangedValues: false,
    hasValues: false,
  }

  componentDidMount() {
    this.props.booksStore.fetchBookGenres();
    this.props.booksStore.fetchBookThemes();

    this.fetchReviews(this.props.match.params);
  }

  componentWillUnmount() {
    this.resetReviews();
  }

  componentDidUpdate(prevProps: Readonly<IProps>): void {
    const {
      reviewsPagesStruct: { data },
    } = this.props.booksStore;
    const {
      reviewsPagesStruct: { data: prevData },
    } = prevProps.booksStore;

    if (toJS(data) !== toJS(prevData)) {
      this.renderedOnce = true;
    }
  }

  fetchReviews = (params: any) => {
    this.props.booksStore.fetchReviewsPagesIfNeeded(params);
  }

  // checkFormValues = (formValues: FormikValues) => !(_.isEmpty(formValues)
  //   || !Object.entries(formValues).find(([, value]) => !_.isEmpty(value)))

  handleFormChange = () => {
    this.setState({
      hasValues: true, // TODO: Check form values
      hasChangedValues: true,
    });
  }

  handleFormSubmit = (formValues: FormikValues) => {
    this.closeFilter();

    this.fetchReviews({
      ...formValues,
      query: this.state.searchQuery,
    });
  }

  handleListBottomIntersection = (inView: boolean) => {
    if (inView) {
      this.props.booksStore.fetchReviewsPagesIfNeeded();
    }
  }

  handleSearchInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      searchQuery: event.target.value,
    });
  }

  handleSearchInputKeydown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (KeyCodes.enter === event.keyCode) {
      this.resetReviews();
      this.submitForm();
    }
  }

  handleSearchInputReset = () => {
    this.setState({
      searchQuery: '',
    });

    this.resetReviewsAndSubmit();
  }

  closeFilter = () => {
    this.setState({
      isFilterOpen: false,
    });

    clearAllBodyScrollLocks();
  }

  toggleFilter = () => {
    this.setState((state) => ({
      isFilterOpen: !state.isFilterOpen,
    }), () => {
      if (!this.sidebarRef.current) {
        return;
      }

      if (this.state.isFilterOpen) {
        disableBodyScroll(this.sidebarRef.current);
      } else {
        clearAllBodyScrollLocks();
      }
    });
  }

  resetReviews = () => {
    this.props.booksStore.reviewsPages.reset();
  }

  resetReviewsAndSubmit = () => {
    this.setState({
      hasChangedValues: false,
    });

    this.resetReviews();
    setTimeout(this.submitForm, 0);
  }

  renderForm = (style?: CSSProperties) => {
    const {
      reviewBookThemesStruct: { data: themes },
      reviewGenresStruct: { data: genres },
      reviewsPagesStruct: { isFetching },
    } = this.props.booksStore;

    const {
      hasValues,
      hasChangedValues,
    } = this.state;

    return (
      <div
        className={cx('form-container')}
        style={{ ...style }}
      >
        <Media maxDeviceWidth={Breakpoints.sm} onChange={this.closeFilter}>
          {(matches) => (
            <Formik
              initialValues={{
                genres: [],
                bookThemes: [],
              }}
              onSubmit={this.handleFormSubmit}
              onReset={() => {
                this.setState({
                  hasChangedValues: true,
                  hasValues: false,
                });

                if (!matches) {
                  this.resetReviewsAndSubmit();
                }
              }}
            >
              {({
                handleReset,
                handleSubmit,
                submitForm,
                values,
              }) => {
                this.submitForm = submitForm;
                this.formValues = values;

                return (
                  <form
                    className={cx('form')}
                    onSubmit={handleSubmit}
                  >
                    {matches && (
                      <div className={cx('top-bar')}>
                        <button
                          className={cx('filters-reset')}
                          type="button"
                          disabled={!hasValues || isFetching}
                          onClick={handleReset}
                        >
                          Очистить
                        </button>
                        <b>Фильтр</b>
                        <Button
                          className={cx('filters-submit')}
                          asLink
                          onClick={hasChangedValues
                            ? this.resetReviewsAndSubmit
                            : this.closeFilter
                          }
                        >
                          {hasChangedValues ? 'Применить' : 'Вернуться'}
                        </Button>
                      </div>
                    )}

                    {genres && (
                      <div className={cx('filters-group')}>
                        {!matches && (
                          <div className={cx('filters-group-header')}>
                            <div className={cx('filters-group-title')}>Жанры</div>
                            <button
                              className={cx('filters-reset')}
                              type="button"
                              disabled={!hasValues || isFetching}
                              onClick={handleReset}
                            >
                              Очистить
                            </button>
                          </div>
                        )}

                        {genres.map((genre: Api.BookGenre) => (
                          <FormControl.FormikCheckbox
                            key={genre.id}
                            name="genres"
                            value={genre.id}
                            label={genre.name}
                            id={`genre${genre.id}`}
                            labelClassName={cx('checkbox-label')}
                            controlClassName={cx('checkbox-control')}
                            color='primary'
                            reverse
                            spread
                            onChange={() => {
                              this.handleFormChange();

                              if (!matches) {
                                this.resetReviewsAndSubmit();
                              }
                            }}
                          />
                        ))}
                      </div>
                    )}

                    {themes && (
                      <div className={cx('filters-group')}>
                        {!matches && (
                          <div className={cx('filters-group-header')}>
                            <div className={cx('filters-group-title')}>Тематика</div>
                          </div>
                        )}
                        {themes.map((theme: Api.BookTheme) => (
                          <FormControl.FormikCheckbox
                            key={theme.id}
                            name="bookThemes"
                            value={theme.id}
                            label={theme.name}
                            id={`theme${theme.id}`}
                            labelClassName={cx('checkbox-label')}
                            controlClassName={cx('checkbox-control')}
                            color='primary'
                            reverse
                            spread
                            onChange={() => {
                              this.handleFormChange();

                              if (!matches) {
                                this.resetReviewsAndSubmit();
                              }
                            }}
                          />
                        ))}
                      </div>
                    )}
                  </form>
                );
              }}
            </Formik>
          )}
        </Media>
      </div>
    );
  }

  renderSidebar = () => {
    const {
      reviewBookThemesStruct: { data: themes, isFetching: isThemesFetching },
      reviewGenresStruct: { data: genres, isFetching: isGenresFetching },
    } = this.props.booksStore;
    const {
      isFilterOpen,
    } = this.state;

    if ((!themes || isThemesFetching) || (!genres || isGenresFetching)) {
      return <BookReviewFormLoader />;
    }

    return (
      <div
        className={cx('left-container', { open: isFilterOpen })}
        ref={this.sidebarRef}
      >
        <Media maxDeviceWidth={Breakpoints.sm}>
          {(matches) => (matches
            ? this.renderForm()
            : (
              <Sticky topOffset={-88} bottomOffset={98}>
                {({ style }) => {
                  let top = 88;

                  if (style.top && style.top < 0) {
                    top = 88 + Number(style.top);
                  }

                  return this.renderForm({ ...style, top });
                }}
              </Sticky>
            ))}
        </Media>
      </div>
    );
  }

  renderContent = () => {
    const {
      reviewsPages: { data: reviews, reseted },
      reviewsPagesStruct: { data, isFetching },
    } = this.props.booksStore;

    const {
      searchQuery,
    } = this.state;

    if (!reviews.length && isFetching && !this.renderedOnce) {
      return <BookReviewLoader />;
    }

    return (
      <div className={cx('right-container')}>
        <Title as="h2" className="is-hidden-xs">Обзоры книг</Title>
        <div className={cx('search-input-container', 'is-grow-mobile', {
          revealed: !_.isEmpty(searchQuery),
        })}>
          <input
            type="text"
            className={cx('search-input')}
            placeholder="Поиск по названию"
            value={searchQuery}
            onChange={this.handleSearchInputChange}
            onInput={this.handleSearchInputChange}
            onKeyDown={this.handleSearchInputKeydown}
          />
          <Icon
            name="times-blue-sm"
            className={cx('search-input-icon')}
            onClick={this.handleSearchInputReset}
          />
        </div>

        {
          (reseted || !reviews || !data || isFetching)
            ? <BookReviewListLoader />
            : (
              <>
                <CardsList.Container className={cx('cards')} maxItemsPerRow={8}>
                  {reviews.map((review, index) => (
                    <CardsList.Item key={index}>
                      <ReviewCard item={review} />
                    </CardsList.Item>
                  ))}
                </CardsList.Container>
                <InView onChange={this.handleListBottomIntersection} rootMargin="100px">
                  <div className={cx('list-bottom')} />
                </InView>
                {!reviews.length && <p className={cx('not-found')}>Не найдено материалов</p>}
              </>
            )
        }
      </div>
    );
  }

  render() {
    return (
      <div className={cx('content-container')}>
        <StickyContainer className={cx('content')}>
          {this.renderSidebar()}
          {this.renderContent()}
        </StickyContainer>
        <Media maxDeviceWidth={Breakpoints.sm} onChange={this.closeFilter}>
          {(matches) => matches && (
            <div className={cx('bottom-bar')}>
              <button onClick={this.toggleFilter} className={cx('bottom-bar-button')}>
                <span>Фильтр</span>
              </button>
            </div>
          )}
        </Media>
      </div>
    );
  }
}
