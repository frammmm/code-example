import React, { GetDerivedStateFromProps } from 'react';
import Media from 'react-responsive';
import { Link, RouteComponentProps } from 'react-router-dom';
import { StickyContainer, Sticky } from 'react-sticky';
import { InView } from 'react-intersection-observer';
import { inject, observer } from 'mobx-react';
import * as _ from 'lodash';
import cn from 'classnames/bind';
/* eslint-disable import/no-duplicates */
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
/* eslint-enable */

import { Api, Stores } from 'types';
import {
  Breadcrumbs, Button, ButtonGroup, CardsList, ContentWrapper, EditionLoader, IssueCard, Title,
} from 'components';
import { Breakpoints } from 'utils/constants';
import { makeResizeUrl } from 'utils/imageResize';

import styles from './index.module.scss';

const cx = cn.bind(styles);

interface RouteParams {
  editionId: string;
}

interface IProps extends RouteComponentProps<RouteParams> {
  periodicalsStore: Stores.IPeriodicalsStore;
}

interface IState {
  editionCollected: boolean | undefined;
}

@inject('periodicalsStore')
@observer
export class Edition extends React.Component<IProps, IState> {
  state = {
    editionCollected: undefined,
  }

  static getDerivedStateFromProps: GetDerivedStateFromProps<IProps, IState> =
  (props: IProps, prevState: IState) => {
    const { editionStruct: { data } } = props.periodicalsStore;
    const isCurrentCollected = _.get(data, ['data', 'isCollected']);

    if (!_.isNil(prevState.editionCollected)) {
      return prevState;
    }

    return {
      editionCollected: isCurrentCollected,
    };
  }

  componentDidMount() {
    const { editionId } = this.props.match.params;
    const { editionStruct: { data, isFetching } } = this.props.periodicalsStore;

    const isEditionChanged = Number(editionId) !== _.get(data, 'id');

    if ((!data || isEditionChanged) && !isFetching) {
      this.props.periodicalsStore.fetchEdition(editionId);
      this.props.periodicalsStore.fetchEditionPeriodicsIfNeeded(editionId);
    }
  }

  componentWillUnmount() {
    this.props.periodicalsStore.clearStructs();
  }

  handleListBottomIntersection = (inView: boolean) => {
    if (inView) {
      const { editionId } = this.props.match.params;

      this.props.periodicalsStore.fetchEditionPeriodicsIfNeeded(editionId);
    }
  }

  handleCollectButtonClick = () => {
    const { editionId } = this.props.match.params;

    const { editionCollected } = this.state;
    const nextCollectedState = !editionCollected;

    this.setState({
      editionCollected: nextCollectedState,
    });

    this.props.periodicalsStore.toggleEditionCollectionState(editionId, nextCollectedState);
  }

  renderBreadcrumbs() {
    const {
      editionStruct: { data: editionData },
    } = this.props.periodicalsStore;

    const title = _.get(editionData, ['data', 'name']);

    return (
      <Breadcrumbs>
        <Link to="/">Периодика</Link>
        <Link to="/branches">Рубрики</Link>
        <span>{title}</span>
      </Breadcrumbs>
    );
  }

  renderCards(editionPeriods: Api.EditionPeriod[]) {
    return editionPeriods.map(({
      issues, index, endDate, startDate,
    }: any) => {
      const sectionStartDate = format(new Date(startDate), 'd MMMM', { locale: ru });
      const sectionEndDate = format(new Date(endDate), 'd MMMM', { locale: ru });

      return (
        <div className={cx('section')} key={index}>
          <div className={cx('section-title')}>{sectionStartDate} - {sectionEndDate}</div>
          <CardsList.Container mobileThreeColumnLayout>
            {issues.map((item: Api.Issue) => (
              <CardsList.Item key={item.id}>
                <IssueCard
                  item={item}
                  responsiveMobile
                  renderDate
                />
              </CardsList.Item>
            ))}
          </CardsList.Container>
          <InView onChange={this.handleListBottomIntersection} rootMargin="100px">
            <div className={cx('list-bottom')} />
          </InView>
        </div>
      );
    });
  }

  renderDescription() {
    const {
      editionStruct: { data: editionData },
      editionCollectStruct: { isFetching },
    } = this.props.periodicalsStore;

    const title = _.get(editionData, ['data', 'name']);
    const description = _.get(editionData, ['data', 'description']);

    const { editionCollected } = this.state;

    return (
      <>
        <Title as="h2" className={cx('title')}>{title}</Title>
        <p className={cx('description')}>
          {description}
        </p>
        <ButtonGroup inline>
          <Button
            className={cx('is-grow-mobile', {
              collect: editionCollected,
            })}
            color={editionCollected && !isFetching ? 'secondary' : 'primary'}
            onClick={this.handleCollectButtonClick}
            loading={isFetching}
            id="test-toggle-edition-collected"
          >
            {editionCollected ? 'Удалить из коллекции' : 'Добавить в мои коллекции'}
          </Button>
          {/* <Button */}
          {/*   color="secondary" */}
          {/*   className={cx({ notification: true })} */}
          {/*   onClick={console.log} */}
          {/* /> */}
        </ButtonGroup>
      </>
    );
  }

  renderDesktop() {
    const { editionPeriods } = this.props.periodicalsStore;

    const editionPeriodsData = _.get(editionPeriods, ['data']);
    const latestIssue = _.get(editionPeriodsData, [0, 'issues', 0]);

    return (
      <div className={cx('container')}>
        <div className={cx('content')}>
          {this.renderBreadcrumbs()}

          <ContentWrapper className={cx('content-inner')}>
            <div className={cx('left-container')}>
              {latestIssue && (
                <StickyContainer>
                  <Sticky topOffset={-90}>
                    {({ style, isSticky }) => (
                      <Link
                        to={latestIssue.url}
                        style={{ ...style, top: isSticky ? 90 : 0 }}
                        id="test-latest-issue-link"
                      >
                        <img src={makeResizeUrl(latestIssue.cover, 356)} className={cx('cover')} alt="latest issue" />
                      </Link>
                    )}
                  </Sticky>
                </StickyContainer>
              )}
            </div>
            <div className={cx('right-container')}>
              {this.renderDescription()}

              <div className={cx('sections-container')}>
                {this.renderCards(editionPeriodsData)}
              </div>
            </div>
          </ContentWrapper>
        </div>
      </div>
    );
  }

  renderMobile() {
    const { editionPeriods } = this.props.periodicalsStore;
    const editionPeriodsData = _.get(editionPeriods, ['data']);
    const latestIssue = _.get(editionPeriodsData, [0, 'issues', 0]);

    return (
      <div className={cx('container')}>
        <div className={cx('content')}>
          {this.renderBreadcrumbs()}

          <div className={cx('content-inner')}>
            <ContentWrapper>
              {this.renderDescription()}

              {latestIssue && (
                <Link
                  className={cx('cover-container')}
                  to={latestIssue.url}
                  id="test-latest-issue-link"
                >
                  <img src={makeResizeUrl(latestIssue.cover, 356)} className={cx('cover')} alt="latest issue" />
                  <div className={cx('latest-issue')}>Читать последний выпуск</div>
                </Link>
              )}

              <div className={cx('sections-container')}>
                <Title as="h2">Выпуски</Title>
                {this.renderCards(editionPeriodsData)}
              </div>
            </ContentWrapper>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const {
      editionPeriods: { data },
      editionPeriodsStruct: { isFetching: periodsIsFetching },
      editionStruct: { data: editionData, isFetching: editionIsFetching },
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      editionCollectStruct: { isFetching },
    } = this.props.periodicalsStore;

    if ((!data.length && periodsIsFetching) || !editionData || editionIsFetching) {
      return (
        <div className={cx('container')}>
          <div className={cx('content')}>
            <EditionLoader />
          </div>
        </div>
      );
    }

    return (
      <Media maxDeviceWidth={Breakpoints.sm}>
        {(matches) => (matches ? this.renderMobile() : this.renderDesktop())}
      </Media>
    );
  }
}
