import React from 'react';
import * as _ from 'lodash';
import { Link } from 'react-router-dom';
import cn from 'classnames/bind';
import { inject, observer } from 'mobx-react';
import {
  Button, Form, FormControl, Title,
} from 'components';
import styles from './index.module.scss';

const cx = cn.bind(styles);

const getErrorMessage = (errors: any) => {
  const msg = _.get(errors, ['data', 'message']);
  return _.isString(msg) ? msg : '';
};

interface IProps {
  children?: React.ReactNode;
  authStore?: any;
}

interface IState {
  isAgreementConfirmed: boolean;
}

@inject('authStore')
@observer
export class Login extends React.Component<IProps, IState> {
  state = {
    isAgreementConfirmed: false,
  }

  private handleAgreementChange = () => {
    this.setState((state) => ({
      isAgreementConfirmed: !state.isAgreementConfirmed,
    }));
  }

  private handleSubmitEmail = (formValues: any) => {
    this.props.authStore.submitEmail(formValues);
  }

  private handleSubmitCode = (formValues: any) => {
    if (formValues.code) {
      this.props.authStore.submitCode(formValues);
    } else {
      this.handleSubmitEmail(formValues);
    }
  }

  private renderEmailForm = () => {
    const { isAgreementConfirmed } = this.state;
    const {
      authStore: {
        loginEmailStruct: { isFetching, errors },
      },
    } = this.props;
    const errorMessage = getErrorMessage(errors);
    return (
      <Form className={cx('form')} onSubmit={this.handleSubmitEmail}>
        <Title as="h2" className={cx('title')}>
          Вход
        </Title>
        <FormControl.Text
          type="email"
          name="email"
          id="login-form-email"
          placeholder="Введите e-mail"
          error={errorMessage}
        />
        <Button
          type="submit"
          fullwidth
          disabled={!isAgreementConfirmed || isFetching}
          loading={isFetching}
          id="test-submit-email"
        >
                    Далее
        </Button>
        <FormControl.Checkbox
          name="agreement"
          id="login-form-agreement"
          checked={isAgreementConfirmed}
          onChange={this.handleAgreementChange}
          label={
            <>
              <span>Я согласен с&nbsp;</span>
              <Link to="/agreement">правилами пользования</Link>
              &nbsp;и<br />
              <a
                href="..."
                target="_blank"
                rel="noopener noreferrer"
              >
                {'политикой конфиденциальности.'}
              </a>
            </>
          }
        />
      </Form>
    );
  };

  private renderCodeForm = () => {
    const {
      authStore: {
        temporaryEmail,
        loginCodeStruct: { isFetching, errors: codeErrors },
        loginEmailStruct: { errors: emailErrors },
      },
    } = this.props;
    const codeErrorMessage = getErrorMessage(codeErrors);
    const emailErrorMessage = getErrorMessage(emailErrors);
    return (
      <Form className={cx('form')} onSubmit={this.handleSubmitCode}>
        <Title as="h2" className={cx('title')}>
          Вход
        </Title>
        <FormControl.Text
          type="email"
          name="email"
          id="login-form-email"
          defaultValue={temporaryEmail}
          placeholder="Введите e-mail"
          error={emailErrorMessage}
        />
        <FormControl.Text
          autoFocus
          name="code"
          id="login-form-code"
          placeholder="Введите код"
          error={codeErrorMessage}
        />
        <Button
          type="submit"
          fullwidth
          disabled={isFetching}
          loading={isFetching}
          id="test-submit-code"
        >
          Войти
        </Button>
        {!codeErrors && !emailErrors && (
          <span className={cx('text-success')}>
            На адрес электронной почты {temporaryEmail} было выслано письмо с
            кодом
          </span>
        )}
      </Form>
    );
  }

  render() {
    const {
      authStore: { temporaryEmail },
    } = this.props;
    return (
      <div className={cx('container')}>
        {!temporaryEmail ? this.renderEmailForm() : this.renderCodeForm()}
      </div>
    );
  }
}
