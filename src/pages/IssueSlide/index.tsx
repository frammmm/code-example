import React from 'react';
import cn from 'classnames/bind';
import * as _ from 'lodash';
import { Link } from 'react-router-dom';
import { Redirect, withRouter, RouteComponentProps } from 'react-router';
import Media from 'react-responsive';
import { inject, observer } from 'mobx-react';
import qs from 'qs';
import { Api } from 'types';
import { history } from 'utils/history';
import { makeIssueDocUrl } from 'utils/urls';
import { mixpanel } from 'utils/mixpanel';
import { trackStatEvent } from 'utils/analytics';
import {
  BottomFixedBar, Button, ContentWrapper, Icon, ImageWithCoordNav, Slider, Spinner, Tooltip,
} from 'components';
import { SpreadPan } from 'containers';
import { Breakpoints, KeyCodes } from 'utils/constants';
import styles from './index.module.scss';

const cx = cn.bind(styles);

const MIN_SCALE = 1;
const MAX_SCALE = 3;

interface IProps {
  issueId: string;
  editionId: string;
  children?: React.ReactNode;
  page?: 'left' | 'right';
  periodicalsStore?: any;
  spreadIdx: string;
}

interface IState {
  isAgreementConfirmed: boolean;
  scale: number;
  showPostsList: boolean;
}

type TIssueSlide = IProps & RouteComponentProps & {
  isMobile: boolean;
}

@(withRouter as any)
@inject('periodicalsStore')
@observer
export class IssueSlideComponent extends React.Component<TIssueSlide, IState> {
  spreadRef = React.createRef<HTMLDivElement>()

  state = {
    isAgreementConfirmed: false,
    scale: 1,
    showPostsList: false,
  }

  constructor(props: TIssueSlide) {
    super(props);

    if (props.page === 'right') {
      this.state = {
        ...this.state,
      };
    }
  }

  componentDidMount() {
    if (this.props.periodicalsStore.issueStruct.data) { this.trackDocumentOpen(); }

    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentDidUpdate(prevProps: IProps) {
    if (prevProps.spreadIdx !== this.props.spreadIdx) {
      this.setState({ scale: 1 });
    }

    if (this.props.periodicalsStore.issueStruct.data) { this.trackDocumentOpen(); }

    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  get isMobileLeftImage() {
    return this.props.page !== 'right';
  }

  get spreadIdx() {
    return Number(this.props.spreadIdx);
  }

  get pagesCount() {
    return this.props.periodicalsStore.issueStruct.data.pages.length;
  }

  get isFirstSpread() {
    return this.spreadIdx === 0;
  }

  get isLastSpread() {
    return this.spreadIdx === Math.floor(this.pagesCount / 2);
  }

  get isLastPage() {
    return this.spreadIdx === Math.floor(this.pagesCount / 2) && !this.isMobileLeftImage;
  }

  trackDocumentOpen = _.once(() => {
    const {
      editionId,
      issueId,
      periodicalsStore: { issueStruct: { data } },
    } = this.props;

    const pagesIds = this.getSpreadPages()
      .filter(_.identity)
      .map(({ num }) => num);

    trackStatEvent('spread', {
      issueId: Number(issueId),
      pages: pagesIds,
    });
    mixpanel.track('spread', {
      editionId: Number(editionId),
      issueId: Number(issueId),
      issueName: `${data.edition.name}(${data.number})`,
      pages: pagesIds,
    });
  });

  handleChangeScale = (scale: number) => { this.setState({ scale }); }

  handleKeyDown = (event: KeyboardEvent) => {
    if ([KeyCodes.arrowLeft, KeyCodes.arrowRight].includes(event.keyCode)) {
      const direction = event.keyCode === KeyCodes.arrowLeft ? 'prev' : 'next';

      if (this.props.isMobile) {
        this.handleMobileNavigation(direction);
      } else {
        this.handleNavigation(direction);
      }
    }
  }

  handleTipShow = () => {
    if (!this.state.showPostsList) {
      this.setState({ showPostsList: true });
    }
  }

  handleTipHide = () => { this.setState({ showPostsList: false }); }

  handleAgreementChange = () => {
    this.setState((state) => ({
      isAgreementConfirmed: !state.isAgreementConfirmed,
    }));
  }

  handleNavigation = (direction: 'prev' | 'next') => {
    const { search } = this.props.location;
    const params = qs.parse(search.substr(1));
    params.index = Number(params.index);

    if ((direction === 'prev' && params.index === 0)
      || (direction === 'next' && this.isLastSpread)) {
      return;
    }

    params.index += direction === 'prev' ? -1 : 1;
    delete params.page;

    history.push(`?${qs.stringify(params)}`);
  }

  handleMobileNavigation = (direction: 'prev' | 'next') => {
    const { search } = this.props.location;

    const params = qs.parse(search.substr(1));
    params.index = Number(params.index);

    if ((direction === 'prev' && params.index === 0)
      || (direction === 'next' && this.isLastPage)) {
      return;
    }

    if (params.index === 1 && direction === 'prev' && this.isMobileLeftImage) {
      params.index = 0;
      delete params.page;
    } else if (params.index === 0 && direction === 'next') {
      params.index = 1;
      params.page = 'left';
    } else {
      const changeSpreadIndex = (this.isMobileLeftImage && direction === 'prev')
        || ((!this.isMobileLeftImage || params.index === 0) && direction === 'next');


      if (changeSpreadIndex) {
        params.index += direction === 'prev' ? -1 : 1;
      }

      params.page = this.isMobileLeftImage ? 'right' : 'left';
    }

    history.push(`?${qs.stringify(params)}`);
  }

  isPointInRect = ({ x, y }: any, region: Api.IssueDetails.Coordinate) => (
    x >= region.top
    && x <= region.bottom
    && y >= region.left
    && y <= region.right
  )

  handleClickRegion = ({ x, y, pageNum }: { x: number; y: number; pageNum: number }) => {
    const { issueStruct: { data: issueData } } = this.props.periodicalsStore;
    if (issueData) {
      const { documents } = issueData;

      const doc = documents.find(({ pages, coordinates }: any) => {
        if (!pages.includes(pageNum)) {
          return false;
        }
        return coordinates.find(
          (c: Api.IssueDetails.Coordinate) => this.isPointInRect({ x, y }, c),
        );
      });

      if (doc) {
        const { editionId, issueId } = this.props;
        history.push(makeIssueDocUrl({ editionId, issueId, id: doc.id }));
      }
    }
  }

  getSpreadPages = () => {
    const { periodicalsStore } = this.props;
    const { issueStruct: { data: issueData } } = periodicalsStore;

    if (this.spreadIdx === 0) {
      return [{ num: 1, imageUrl: issueData.pages[0] }];
    }

    if (this.spreadIdx > Math.floor(issueData.pages.length / 2)) {
      return [];
    }

    const leftPageNum = this.spreadIdx * 2;
    const rightPageNum = this.spreadIdx * 2 + 1;
    const leftPageUrl = issueData.pages[leftPageNum - 1];
    const rightPageUrl = issueData.pages[rightPageNum - 1];

    if (!rightPageUrl) {
      return [{
        num: leftPageNum, imageUrl: leftPageUrl,
      }];
    }

    return [
      { num: leftPageNum, imageUrl: leftPageUrl },
      { num: rightPageNum, imageUrl: rightPageUrl },
    ];
  }

  getMobilePage = () => {
    const [leftPage, rightPage] = this.getSpreadPages();
    const { page } = this.props;

    return page === 'right' ? rightPage : leftPage;
  }

  renderPostsList = () => {
    const { editionId, issueId } = this.props;
    const { issueStruct: { data: issueData } } = this.props.periodicalsStore;

    if (!issueData) {
      return null as any;
    }

    const { documents } = issueData;
    const [leftPage, rightPage] = this.getSpreadPages();
    const docs = documents.filter(({ pages }: any) => (
      pages.includes(leftPage.num) || (rightPage && pages.includes(rightPage.num))
    ));

    if (_.isEmpty(docs)) {
      return null as any;
    }

    const sortedDocs = _.sortBy(
      docs,
      ({ pages }: any) => (pages.includes(leftPage.num) ? 0 : 1),
    );
    return (
      <>
        <div className={cx('tooltip-title')}>
          {'Какую статью вы хотите открыть?'}
        </div>
        <ul className={cx('tooltip-posts-list')} id="test-spread-docs-list">
          {sortedDocs.map(({ title, id }: any) => (
            <li key={id}>
              <Link to={makeIssueDocUrl({ editionId, issueId, id })}>
                {title}
              </Link>
            </li>
          ))}
        </ul>
      </>
    );
  }

  renderBottomBar = () => {
    const { scale, showPostsList } = this.state;
    const tooltipContent = this.renderPostsList();
    return (
      <Media maxDeviceWidth={Breakpoints.sm}>
        {(matches) => (
          <BottomFixedBar className={cx('bottom-bar')}>
            <div className={cx('bottom-bar-column', 'is-hidden-xs')}>
              <div className={cx('bottom-bar-slider')}>
                <Icon name="aA" />
                <Slider
                  min={MIN_SCALE}
                  max={MAX_SCALE}
                  value={scale}
                  step={0.2}
                  className={cx('slider')}
                  onChange={this.handleChangeScale}
                />
              </div>
            </div>
            <div className={cx('bottom-bar-column-container')}>
              <div className={cx('bottom-bar-column')}>
                {tooltipContent && (
                  <>
                    <Icon name="lines" className={cx('bottom-bar-icon')} />
                    <Tooltip
                      className={cx('bottom-bar-tooltip')}
                      content={tooltipContent}
                      onHidden={this.handleTipHide}
                      visible={!!showPostsList}
                      maxWidth="500px"
                    >
                      <span
                        className={cx('bottom-bar-text')}
                        onClick={this.handleTipShow}
                      >
                        {matches ? '...' : '...'}
                      </span>
                    </Tooltip>
                  </>
                )}
              </div>
              <div className={cx('bottom-bar-column')}>
                <Link to="?view=preview" className={cx('bottom-bar-link')} id="test-close-spread-link">
                  <span className={cx('bottom-bar-text', 'semibold')}>Закрыть</span>
                  <Icon className={cx('bottom-bar-right-icon')} name={matches ? 'times-black-sm' : 'times-blue-sm'}/>
                </Link>
              </div>
            </div>
          </BottomFixedBar>
        )}
      </Media>
    );
  }

  renderDesktop = () => {
    const { scale } = this.state;
    const [leftPage, rightPage] = this.getSpreadPages();
    if (!leftPage && !rightPage) {
      return (
        <Redirect to="./?view=preview" />
      );
    }
    return (
      <div className={cx('container')}>
        <div className={cx('sidebar', 'left')}>
          {!this.isFirstSpread && (
            <Button
              className={cx('sidebar-link')}
              id="test-prev-spread"
              color="transparent"
              onClick={() => this.handleNavigation('prev')}
            >
              <Icon name="chevron-left-gray" />
            </Button>
          )}
        </div>
        <SpreadPan
          identity={this.spreadIdx}
          scale={scale}
          minScale={MIN_SCALE}
          maxScale={MAX_SCALE}
          className={cx('content-container')}
          onChangeScale={this.handleChangeScale}
        >
          <ImageWithCoordNav
            imageUrl={leftPage.imageUrl}
            pageNum={leftPage.num}
            onClickRegion={this.handleClickRegion}
          />
          {rightPage && (
            <ImageWithCoordNav
              imageUrl={rightPage.imageUrl}
              pageNum={rightPage.num}
              onClickRegion={this.handleClickRegion}
            />
          )}
        </SpreadPan>
        <div className={cx('sidebar', 'right')}>
          {!this.isLastSpread && (
            <Button
              className={cx('sidebar-link')}
              id="test-next-spread"
              color="transparent"
              onClick={() => this.handleNavigation('next')}
            >
              <Icon name="chevron-right-gray" />
            </Button>
          )}
        </div>
        {this.renderBottomBar()}
      </div>
    );
  }

  renderMobileControls = () => (
    <div className={cx('controls')}>
      {!this.isFirstSpread && (
        <Button
          color="white"
          round="xl"
          onClick={() => this.handleMobileNavigation('prev')}
          id="test-prev-spread"
        >
          <Icon name="chevron-left-gray" />
        </Button>
      )}
      {!this.isLastPage && (
        <Button
          className={cx('left')}
          color="white"
          round="xl"
          onClick={() => this.handleMobileNavigation('next')}
          id="test-next-spread"
        >
          <Icon name="chevron-right-gray" />
        </Button>
      )}
    </div>
  )

  renderMobile = () => {
    const mobilePage = this.getMobilePage();

    if (!mobilePage) {
      return (
        <Redirect to="./?view=preview" />
      );
    }

    const { search } = this.props.location;
    const { imageUrl, num } = mobilePage;

    return (
      <div className={cx('container')} key={search}>
        <div className={cx('content-container')}>
          <ImageWithCoordNav
            mobile
            imageUrl={imageUrl}
            pageNum={num}
            onClickRegion={this.handleClickRegion}
          />
          {this.renderMobileControls()}
          {this.renderBottomBar()}
        </div>
      </div>
    );
  }

  render() {
    const { isMobile, periodicalsStore } = this.props;
    const {
      issueStruct: { data: issueData, isFetching: isIssueFetching },
    } = periodicalsStore;

    if (!issueData || isIssueFetching) {
      return (
        <div className={cx('container')}>
          <ContentWrapper className={cx('content-container')}>
            <Spinner />
          </ContentWrapper>
        </div>
      );
    }

    const totalSpreadsCount = (issueData && Math.ceil(issueData.pages.length / 2)) || 0;

    if (totalSpreadsCount === 0 || this.spreadIdx < 0 || this.spreadIdx > totalSpreadsCount) {
      return <Redirect to="./" />;
    }

    return isMobile ? this.renderMobile() : this.renderDesktop();
  }
}

export const IssueSlide: React.FC<IProps> = ((props: IProps) => (
  <Media maxDeviceWidth={Breakpoints.sm}>
    {(matches) => <IssueSlideComponent {...(props as any)} isMobile={matches} />}
  </Media>
));
