import React from 'react';
import { InView } from 'react-intersection-observer';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import cn from 'classnames/bind';
import { Api, Stores } from 'types';
import {
  Breadcrumbs, CardsList, ContentWrapper, EditionCard, Section, SectionLoader,
} from 'components';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  children?: React.ReactNode;
  periodicalsStore: Stores.IPeriodicalsStore;
}

@inject('periodicalsStore')
@observer
export class Branches extends React.Component<IProps> {
  public componentDidMount() {
    this.props.periodicalsStore.fetchBranchesIfNeeded();
  }

  handleListBottomIntersection = (inView: boolean) => {
    if (inView) {
      this.props.periodicalsStore.fetchBranchesIfNeeded();
    }
  }

  public render() {
    const {
      branches: { data: branches },
      branchesStruct: { isFetching },
    } = this.props.periodicalsStore;

    if (!branches.length && isFetching) {
      return (
        <div className={cx('content-container')}>
          <SectionLoader breadcrumbs />
        </div>
      );
    }

    return (
      <div className={cx('content-container')}>
        <Breadcrumbs>
          <Link to="/">Периодика</Link>
          <span>Рубрики</span>
        </Breadcrumbs>
        {branches!.map((branch: Api.Branch) => {
          const link = (
            <Link to={branch.url}>
              <span className='is-hidden-xs'>
                {'Показать все'}
              </span>
              <span className='is-visible-xs'>
                {'Все'}
              </span>
            </Link>
          );
          return (
            <Section
              title={branch.name}
              separator
              key={branch.id}
              link={link}
            >
              <ContentWrapper>
                <CardsList.Container>
                  {
                    branch.editions
                    // FIXME: move to transformResponse
                      .filter(({ latestIssue }) => (latestIssue !== null))
                      .map((item: Api.Edition) => (
                        <CardsList.Item key={item.id}>
                          <EditionCard item={item} responsiveMobile />
                        </CardsList.Item>
                      ))
                  }
                </CardsList.Container>
              </ContentWrapper>
            </Section>
          );
        })}
        <InView onChange={this.handleListBottomIntersection} rootMargin="100px">
          <div className={cx('list-bottom')} />
        </InView>
      </div>
    );
  }
}
