import React from 'react';
import { inject, observer } from 'mobx-react';
import { Link, RouteComponentProps } from 'react-router-dom';
import * as _ from 'lodash';
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
import cn from 'classnames/bind';
import qs from 'qs';
import { Api } from 'types';
import {
  CardsCarousel, EditionCard, Title, ContentWrapper, SearchLoader,
} from 'components';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface RouteParams {
  editionId: string;
}

interface IProps extends RouteComponentProps<RouteParams> {
  children?: React.ReactNode;
  searchStore?: any;
}

@inject('searchStore')
@observer
export class Search extends React.Component<IProps> {
  componentDidMount() {
    this.fetchQuery();
  }

  componentDidUpdate(prevProps: IProps) {
    if (prevProps.location.search !== this.props.location.search) {
      this.fetchQuery();
    }
  }

  getQuery = () => {
    const { location: { search } } = this.props;
    if (!search) {
      return '';
    }
    const { query } = qs.parse(search.slice(1));
    return decodeURIComponent(query);
  }

  fetchQuery = () => {
    const query = this.getQuery();
    if (query) {
      this.props.searchStore.search(query);
    }
  }

  render() {
    const { searchFormStruct: { data, isFetching } } = this.props.searchStore;
    const query = this.getQuery();

    if ((!data && query) || isFetching) {
      return (
        <div className={cx('container')}>
          <div className={cx('content-container')}>
            <SearchLoader />
          </div>
        </div>
      );
    }

    if (!data) {
      return 'empty query state';
    }

    const { editions = [], documents = [] } = data.data;

    return (
      <div className={cx('container')}>
        <div className={cx('content-container')}>
          <ContentWrapper>
            <Title as="h2" className={cx('title')}>Поиск</Title>

            {!_.isEmpty(editions) && (
              <ContentWrapper wide>
                <div className={cx('slider-container')}>
                  <ContentWrapper>
                    <Title as="h5" className={cx('subtitle')}>{`Найдено изданий: ${editions.length}`}</Title>
                  </ContentWrapper>
                  <CardsCarousel>
                    {editions.map((item: Api.Edition) => <EditionCard item={item} key={item.id} />)}
                  </CardsCarousel>
                </div>
              </ContentWrapper>
            )}

            <span className={cx('document-amount')}>{`Найдено материалов: ${documents.length}`}</span>

            {documents.map((item: Api.Search.Document) => {
              const { title, annotation, body = [] } = item.highlight;
              const titleHtml = title && title.join('');
              const descriptionHtml = annotation || body.join('');
              const date = format(new Date(item.issue.date), 'd MMMM', { locale: ru });

              return (
                <div className={cx('document')} key={item.id} data-test="search-snippet-container">
                  <Link
                    to={`${item.issue.url}/?view=doc&id=${item.id}`}
                    className={cx('document-title')}
                    dangerouslySetInnerHTML={{ __html: titleHtml }}
                  />
                  <div className={cx('document-issue')}>
                    <Link to={item.issue.edition.url}>{item.issue.edition.name}</Link>
                    &nbsp;|&nbsp;
                    <Link to={`${item.issue.url}/?view=text&id=${item.id}`}>&#8470;{item.issue.number}, за {date}</Link>
                  </div>
                  <div className={cx('document-description')} dangerouslySetInnerHTML={{ __html: descriptionHtml }} />
                </div>
              );
            })}
          </ContentWrapper>
        </div>
      </div>
    );
  }
}
