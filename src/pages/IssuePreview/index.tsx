import React from 'react';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import * as _ from 'lodash';
import cn from 'classnames/bind';
import { IssueListWrapper } from 'containers';
import { EditionLoader } from 'components';
import { mixpanel } from 'utils/mixpanel';
import { trackStatEvent } from 'utils/analytics';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  issueId: string;
  editionId: string;
  children?: React.ReactNode;
  periodicalsStore?: any;
}

@inject('periodicalsStore')
@observer
export class IssuePreview extends React.Component<IProps> {
  componentDidMount() {
    const { editionId, issueId } = this.props;
    trackStatEvent('spreads', {
      issueId: Number(issueId),
    });
    mixpanel.track('spreads', {
      editionId: Number(editionId),
      issueId: Number(issueId),
    });
  }

  render() {
    const { issueStruct: { data, isFetching } } = this.props.periodicalsStore;

    if (!data || isFetching) {
      return (
        <EditionLoader buttons={false} description={false} wideCards />
      );
    }

    const {
      thumbnails: [mainThumbUrl, ...restThumbUrls],
      edition: { name: editionName, url: editionUrl },
      number,
      date,
    } = data;

    const spreadThumbs = _.chunk(
      restThumbUrls.map((url: string, id: number) => ({ url, id })),
      2,
    );

    return (
      <IssueListWrapper
        name={editionName}
        url={editionUrl}
        thumbUrl={mainThumbUrl}
        number={number}
        date={date}
      >
        <div className={cx('spreads-container')}>
          {spreadThumbs.map((spread: any, spreadIdx) => (
            <div key={spreadIdx} className={cx('spread')} id="test-preview-container">
              {spread.map(({ url: thumbUrl, id }: any, index: number) => (
                <Link
                  className={cx('spread-page-link')}
                  to={`?view=slide&index=${spreadIdx + 1}&page=${index % 2 ? 'right' : 'left'}`}
                  key={id}
                >
                  <img
                    src={thumbUrl}
                    className={cx('spread-page-img')}
                    alt="page preview"
                  />
                </Link>
              ))}
            </div>
          ))}
        </div>
      </IssueListWrapper>
    );
  }
}
