import React from 'react';
import * as _ from 'lodash';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import cn from 'classnames/bind';
import { Api } from 'types';
import { IssueListWrapper } from 'containers';
import { EditionLoader } from 'components';
import { mixpanel } from 'utils/mixpanel';
import { trackStatEvent } from 'utils/analytics';
import styles from './index.module.scss';

const cx = cn.bind(styles);

interface IProps {
  issueId: string;
  editionId: string;
  children?: React.ReactNode;
  periodicalsStore?: any;
}

@inject('periodicalsStore')
@observer
export class IssueTextList extends React.Component<IProps> {
  componentDidMount() {
    if (this.props.periodicalsStore.issueStruct.data) { this.trackDocumentOpen(); }
  }

  componentDidUpdate() {
    if (this.props.periodicalsStore.issueStruct.data) { this.trackDocumentOpen(); }
  }

  trackDocumentOpen = _.once(() => {
    const {
      editionId,
      issueId,
      periodicalsStore: { issueStruct: { data } },
    } = this.props;

    trackStatEvent('documents', {
      issueId: Number(issueId),
    });
    mixpanel.track('documents', {
      editionId: Number(editionId),
      issueId: Number(issueId),
      issueName: `${data.edition.name}(${data.number})`,
    });
  });

  render() {
    const { issueStruct: { data, isFetching } } = this.props.periodicalsStore;

    if (!data || isFetching) {
      return (
        <EditionLoader buttons={false} description={false} wideCards type="text" />
      );
    }

    const {
      documents,
      thumbnails: [mainThumbUrl],
      edition: { name: editionName, url: editionUrl },
      number,
      date,
    } = data;
    return (
      <IssueListWrapper
        name={editionName}
        url={editionUrl}
        thumbUrl={mainThumbUrl}
        number={number}
        date={date}
      >
        <div className={cx('container')}>
          {documents.map((doc: Api.IssueDetails.Document) => {
            const postUrl = `?view=doc&id=${doc.id}`;
            return (
              <div key={doc.id} className={cx('snippet')}>
                <Link to={postUrl} className={cx('title')} data-test="doc-title-link">{doc.title}</Link>
                <div className={cx('description')}>{doc.annotation}</div>
                <div className={cx('pages')}>Страницы: {doc.pages.join(', ')}</div>
                <Link to={postUrl} className={cx('link')} data-test="doc-read-more-link">...</Link>
              </div>
            );
          })}
        </div>
      </IssueListWrapper>
    );
  }
}
